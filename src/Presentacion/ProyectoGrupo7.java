    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentacion;

import Interfaz.InterfazInicio;
import Modelo.Herramientas.CONSTANTES;
import Modelo.Sistema;

import java.awt.Toolkit;
import java.text.ParseException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javax.swing.ImageIcon;

/**
 *
 * @author Grupo 7 (David Llerena,Ariel Velez,Pedro Conforme)
 */
public class ProyectoGrupo7 extends Application {

    private InterfazInicio PantallaInicio;
    public static volatile Scene Ini = null;
    ImageIcon CursorImg;
    Toolkit tk;

    @Override
    public void start(Stage primaryStage) throws ParseException {
        CursorImg = new ImageIcon(CONSTANTES.RUTA_IMAGENES + "cursor.png");
        tk = Toolkit.getDefaultToolkit();

        Sistema.prueba();
        PantallaInicio = new InterfazInicio();
        Ini = new Scene(PantallaInicio.getRoot());
        primaryStage.setTitle("Photo-Gallery"); //Asigno el titulo a la ventana
        primaryStage.getIcons().add(new Image(CONSTANTES.RUTA_IMAGENES + "logo.png"));  // Cambio el iciono de la ventana
        primaryStage.setScene(Ini);
        primaryStage.show();
        primaryStage.setOnCloseRequest(e -> {
            Platform.exit();
        });
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}

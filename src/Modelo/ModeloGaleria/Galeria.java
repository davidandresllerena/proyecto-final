/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.ModeloGaleria;

import Modelo.Usuario.Usuario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Grupo 7 (David Llerena,Ariel Velez,Pedro Conforme)
 */
public class Galeria implements Serializable {

    private ArrayList<Album> albumes;
    private Usuario Dueno;

    public ArrayList<Album> getAlbumes() {
        return albumes;
    }

    public void setAlbumes(ArrayList<Album> albumes) {
        this.albumes = albumes;
    }

    public Usuario getDueno() {
        return Dueno;
    }

    public void setDueno(Usuario Dueno) {
        this.Dueno = Dueno;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.albumes);
        hash = 89 * hash + Objects.hashCode(this.Dueno);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Galeria other = (Galeria) obj;
        if (!Objects.equals(this.albumes, other.albumes)) {
            return false;
        }
        if (!Objects.equals(this.Dueno, other.Dueno)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Galeria{" + "albumes=" + albumes + ", Dueno=" + Dueno + '}';
    }

}

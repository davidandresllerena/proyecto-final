/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.ModeloGaleria;

import Modelo.Foto.Foto;
import Modelo.Herramientas.CONSTANTES;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Grupo 7 (David Llerena,Ariel Velez,Pedro Conforme)
 */
public class Album implements Serializable {

    private ArrayList<Foto> fotos;
    private LocalDateTime FechaCreacion;
    private String PathPortada;
    private String NombreAlbum;
    private String DescripcionAlbum;
    private int IdDueño;
    private int IdAlbum;
    private static int IdAlbumes;

    public Album(String NombreAlbum, int IdDueño) {
        this.IdDueño = IdDueño;
        this.NombreAlbum = NombreAlbum;
        this.fotos = new ArrayList<>();
        this.FechaCreacion = LocalDateTime.now();
        this.PathPortada = CONSTANTES.RUTA_IMAGENES + "album.png";
        this.IdAlbum = IdAlbumes;
        this.DescripcionAlbum = "";
        IdAlbumes++;
    }

    public Album(String NombreAlbum, int IdDueño, String DescripcionAlbum) {
        this.IdDueño = IdDueño;
        this.NombreAlbum = NombreAlbum;
        this.fotos = new ArrayList<>();
        this.FechaCreacion = LocalDateTime.now();
        this.PathPortada = CONSTANTES.RUTA_IMAGENES + "album.png";
        this.IdAlbum = IdAlbumes;
        this.DescripcionAlbum = DescripcionAlbum;
        IdAlbumes++;
    }

    public Album(String NombreAlbum, int IdDueño, ArrayList<Foto> fotos) {
        this.IdDueño = IdDueño;
        this.NombreAlbum = NombreAlbum;
        this.fotos = new ArrayList<>();
        this.fotos = fotos;
        this.FechaCreacion = LocalDateTime.now();
        this.PathPortada = CONSTANTES.RUTA_IMAGENES + "album.png";
        this.IdAlbum = IdAlbumes;
        IdAlbumes++;
    }

    public String getDescripcionAlbum() {
        return DescripcionAlbum;
    }

    public void setDescripcionAlbum(String DescripcionAlbum) {
        this.DescripcionAlbum = DescripcionAlbum;
    }

    public ArrayList<Foto> getFotos() {
        return fotos;
    }

    public void setFotos(ArrayList<Foto> fotos) {
        this.fotos = fotos;
    }

    public LocalDateTime getFechaCreacion() {
        return FechaCreacion;
    }

    public void setFechaCreacion(LocalDateTime FechaCreacion) {
        this.FechaCreacion = FechaCreacion;
    }

    public String getPathPortada() {
        return PathPortada;
    }

    public void setPathPortada(String PathPortada) {
        this.PathPortada = PathPortada;
    }

    public String getNombreAlbum() {
        return NombreAlbum;
    }

    public void setNombreAlbum(String NombreAlbum) {
        this.NombreAlbum = NombreAlbum;
    }

    public int getIdDueño() {
        return IdDueño;
    }

    public void setIdDueño(int IdDueño) {
        this.IdDueño = IdDueño;
    }

    public int getIdAlbum() {
        return IdAlbum;
    }

    public void setIdAlbum(int IdAlbum) {
        this.IdAlbum = IdAlbum;
    }

    public static int getIdAlbumes() {
        return IdAlbumes;
    }

    public static void setIdAlbumes(int IdAlbumes) {
        Album.IdAlbumes = IdAlbumes;
    }

    public void anadirFoto(Foto f) {
        fotos.add(f);
    }

    @Override
    public String toString() {
        return NombreAlbum.toUpperCase();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.fotos);
        hash = 37 * hash + Objects.hashCode(this.FechaCreacion);
        hash = 37 * hash + Objects.hashCode(this.PathPortada);
        hash = 37 * hash + Objects.hashCode(this.NombreAlbum);
        hash = 37 * hash + Objects.hashCode(this.DescripcionAlbum);
        hash = 37 * hash + this.IdDueño;
        hash = 37 * hash + this.IdAlbum;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Album other = (Album) obj;
        if (this.IdDueño != other.IdDueño) {
            return false;
        }
        if (this.IdAlbum != other.IdAlbum) {
            return false;
        }
        if (!Objects.equals(this.PathPortada, other.PathPortada)) {
            return false;
        }
        if (!Objects.equals(this.NombreAlbum, other.NombreAlbum)) {
            return false;
        }
        if (!Objects.equals(this.DescripcionAlbum, other.DescripcionAlbum)) {
            return false;
        }
        if (!Objects.equals(this.fotos, other.fotos)) {
            return false;
        }
        if (!Objects.equals(this.FechaCreacion, other.FechaCreacion)) {
            return false;
        }
        return true;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Usuario;

import Modelo.Foto.Camara;
import Modelo.Foto.Foto;
import Modelo.ModeloGaleria.Album;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Grupo 7 (David Llerena,Pedro Conforme,Ariel Velez)
 */
public class Usuario implements Serializable {

    private static int idUsarios;
    private int idUsuario;
    private String nombre;
    private String correo;
    private String contrasena;
    private String alias;
    private ArrayList<Album> albumes;
    private ArrayList<Camara> camaras;

    public Usuario(String nombre, String correo, String contraseña, String alias) {
        this.nombre = nombre;
        this.correo = correo;
        this.contrasena = contraseña;
        this.alias = alias;
        this.idUsuario = idUsarios;
        albumes = new ArrayList<>();
        camaras = new ArrayList<>();
        idUsarios++;
    }

    public Usuario(String nombre, String correo, String contraseña, String alias, ArrayList<Album> Albumes) {
        this.nombre = nombre;
        this.correo = correo;
        this.contrasena = contraseña;
        this.alias = alias;
        this.idUsuario = idUsarios;
        Albumes = new ArrayList<>();
        camaras = new ArrayList<>();
        this.albumes = Albumes;
        idUsarios++;
    }

    public void anadirAlbum(Album a) {
        albumes.add(a);
    }

    public void anadirCamara(Camara c) {
        camaras.add(c);
    }

    public ArrayList<Camara> getCamaras() {
        return camaras;
    }

    public void setCamaras(ArrayList<Camara> Camaras) {
        this.camaras = Camaras;
    }

    public void quitarFoto(Foto f) {
        for (Album a : albumes) {
            if (a.getFotos().contains(f)) {
                a.getFotos().remove(f);
            }
        }
    }

    public ArrayList<Album> getAlbumes() {
        return albumes;
    }

    public void setAlbumes(ArrayList<Album> Albumes) {
        this.albumes = Albumes;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int IdUsuario) {
        this.idUsuario = IdUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContraseña() {
        return contrasena;
    }

    public void setContraseña(String contraseña) {
        this.contrasena = contraseña;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.idUsuario;
        hash = 29 * hash + Objects.hashCode(this.nombre);
        hash = 29 * hash + Objects.hashCode(this.correo);
        hash = 29 * hash + Objects.hashCode(this.contrasena);
        hash = 29 * hash + Objects.hashCode(this.alias);
        hash = 29 * hash + Objects.hashCode(this.albumes);
        hash = 29 * hash + Objects.hashCode(this.camaras);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (this.idUsuario != other.idUsuario) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.correo, other.correo)) {
            return false;
        }
        if (!Objects.equals(this.contrasena, other.contrasena)) {
            return false;
        }
        if (!Objects.equals(this.alias, other.alias)) {
            return false;
        }
        if (!Objects.equals(this.albumes, other.albumes)) {
            return false;
        }
        if (!Objects.equals(this.camaras, other.camaras)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Usuario{" + "IdUsuario=" + idUsuario + ", nombre=" + nombre + ", correo=" + correo + ", contrase\u00f1a=" + contrasena + ", alias=" + alias + ", Albumes=" + albumes + ", Camaras=" + camaras + '}';
    }

}

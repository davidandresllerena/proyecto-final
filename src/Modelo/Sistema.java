/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Modelo.Foto.Camara;
import Modelo.Foto.Foto;
import Modelo.Foto.Reaccion;
import Modelo.Herramientas.CONSTANTES;
import Modelo.Herramientas.ManejoDeArchivos;
import Modelo.ModeloGaleria.Album;
import Modelo.Usuario.Usuario;
import java.util.ArrayList;

/**
 *
 * @author Grupo 7 (David Llerena,Pedro Conforme,Ariel Velez)
 */
public class Sistema {

    public static ArrayList<Usuario> ListaUsuarios = ManejoDeArchivos.cargarUsuarios();
    public static ArrayList<Album> ListaAlbumes = ManejoDeArchivos.cargarAlbumes();
    public static ArrayList<Foto> ListaFotos = ManejoDeArchivos.cargarFotos();
    public static ArrayList<Reaccion> ListaReacciones = ManejoDeArchivos.cargarReaccion();

    public static void prueba() {
        //DEJO ESTO DE ABAJO X SI SE DAÑA ALGUN ARCHIVO .DAT DEL PROGRAMA

//        Usuario U1 = new Usuario("David", "david", "12345", "Papu");
//        ListaUsuarios.add(U1);
//        Album A1 = new Album("Prueba 1", U1.getIdUsuario());
//        A1.setDescripcionAlbum("Para prueba #1");
//        Album A2 = new Album("Prueba 2", U1.getIdUsuario());
//        A2.setDescripcionAlbum("Para prueba #2");
//        Album A3 = new Album("Prueba 3", U1.getIdUsuario());
//        A3.setDescripcionAlbum("Para prueba #3");
//        U1.anadirAlbum(A1);
//        U1.anadirAlbum(A2);
//        U1.anadirAlbum(A3);
//        Camara C1 = new Camara("Samsung", "Delux2019");
//        C1.setPathImagen(CONSTANTES.RUTA_IMAGENES + "camarapre.png");
//        U1.anadirCamara(C1);
//        Foto F1 = new Foto("Prueba F1", "Dell1", C1, CONSTANTES.RUTA_IMAGENES + "test1.png");
//        Foto F2 = new Foto("Prueba F2", "Dell2", C1, CONSTANTES.RUTA_IMAGENES + "test2.png");
//        Foto F3 = new Foto("Prueba F3", "Dell3", C1, CONSTANTES.RUTA_IMAGENES + "test3.png");
//        Foto F4 = new Foto("Prueba F4", "Dell4", C1, CONSTANTES.RUTA_IMAGENES + "test4.jpg");
//        Foto F5 = new Foto("Prueba F5", "Dell5", C1, CONSTANTES.RUTA_IMAGENES + "test5.png");
//        Foto F6 = new Foto("Prueba F6", "Dell6", C1, CONSTANTES.RUTA_IMAGENES + "test6.png");
//        Foto F7 = new Foto("Prueba F7", "Dell7", C1, CONSTANTES.RUTA_IMAGENES + "test7.jpg");
//        Foto F8 = new Foto("Prueba F8", "Dell8", C1, CONSTANTES.RUTA_IMAGENES + "test8.jpg");
//        Foto F9 = new Foto("Prueba F9", "Dell9", C1, CONSTANTES.RUTA_IMAGENES + "test9.png");
//        Foto F10 = new Foto("Prueba F10", "Dell10", C1, CONSTANTES.RUTA_IMAGENES + "test10.jpg");
//        F1.anadirhashtag("Cool");
//        F1.anadirhashtag("Prueba");
//        F1.anadirIntegrande("Goku");
//        F1.anadirIntegrande("Vegeta");
//        A1.anadirFoto(F10);
//        A1.anadirFoto(F1);
//        A1.anadirFoto(F2);
//        A1.anadirFoto(F3);
//        A1.anadirFoto(F4);
//        A2.anadirFoto(F5);
//        A2.anadirFoto(F6);
//        A2.anadirFoto(F7);
//        A3.anadirFoto(F8);
//        A3.anadirFoto(F9);
//        Reaccion R1 = new Reaccion("like", CONSTANTES.RUTA_IMAGENES + "like.gif");
//        Reaccion R2 = new Reaccion("love", CONSTANTES.RUTA_IMAGENES + "love.gif");
//        Reaccion R3 = new Reaccion("haha", CONSTANTES.RUTA_IMAGENES + "lol.gif");
//        Reaccion R4 = new Reaccion("wow", CONSTANTES.RUTA_IMAGENES + "surprise.gif");
//        Reaccion R5 = new Reaccion("sad", CONSTANTES.RUTA_IMAGENES + "sad.gif");
//        Reaccion R6 = new Reaccion("angry", CONSTANTES.RUTA_IMAGENES + "angry.gif");
//        ListaReacciones.add(R1);
//        ListaReacciones.add(R2);
//        ListaReacciones.add(R3);
//        ListaReacciones.add(R4);
//        ListaReacciones.add(R5);
//        ListaReacciones.add(R6);
//        ManejoDeArchivos.guardarAlbumes(ListaAlbumes);
//        ManejoDeArchivos.guardarMantenimientos(ListaReacciones);
//        ManejoDeArchivos.guardarSolicitudes(ListaFotos);
//        ManejoDeArchivos.guardarUsuarios(ListaUsuarios);
    }
}

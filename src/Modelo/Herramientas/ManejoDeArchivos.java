/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Herramientas;

import Modelo.Foto.Foto;
import Modelo.Foto.Reaccion;
import Modelo.ModeloGaleria.Album;
import Modelo.Usuario.Usuario;
import java.io.*;
import java.util.ArrayList;
import java.util.logging.Logger;
import javafx.application.Platform;

/**
 *
 * @author Grupo 7 (David Llerena,Ariel Velez,Pedro Conforme)
 */
public class ManejoDeArchivos {

    static final Logger LOGGER = Logger.getAnonymousLogger();
    private static final String FILENAMEUSUARIOS = "src/PersistenciaDeDatos/users.dat";
    private static final String FILENAMEALBUMES = "src/PersistenciaDeDatos/albumes.dat";
    private static final String FILENAMEFOTOS = "src/PersistenciaDeDatos/fotos.dat";
    private static final String FILENAMEREACCIONES = "src/PersistenciaDeDatos/reacciones.dat";

    /**
     * Lee un archivo binario y retorna una arreglo de usuarios
     *
     * @return Una arreglo de usuarios
     */
    public synchronized static ArrayList<Usuario> cargarUsuarios() {
        ArrayList<Usuario> z = new ArrayList<>();
        try (ObjectInputStream objInputStream = new ObjectInputStream(
                new FileInputStream(FILENAMEUSUARIOS))) {
                
                    z = (ArrayList<Usuario>) objInputStream.readObject();
                    //System.out.println(z);
               
        } catch (EOFException eof) {
            LOGGER.config("Se cargaron los usuarios...");
        } catch (ClassNotFoundException ex) {
            LOGGER.config("Corrupted file");
        }

         catch (FileNotFoundException e1) {
            LOGGER.config(e1.getMessage());
            Platform.exit();
        } catch (IOException e2) {
            LOGGER.config(e2.getMessage());
            Platform.exit();
        }
        return z;
    }

    /**
     * Sobree escibre un archivo binario con una arreglo de Usuarios
     *
     * @param ListaUsuarios la lista a escribirse
     */
    public synchronized static void guardarUsuarios(ArrayList<Usuario> ListaUsuarios) {
        try (ObjectOutputStream objOutputStream = new ObjectOutputStream(
                new FileOutputStream(FILENAMEUSUARIOS))) {
            objOutputStream.writeObject(ListaUsuarios);
            LOGGER.config("... written to users.dat.");

        } catch (FileNotFoundException e1) {
            LOGGER.config(e1.getMessage());
        } catch (IOException e2) {
            LOGGER.config(e2.getMessage());
        }
    }

    public synchronized static ArrayList<Album> cargarAlbumes() {
        ArrayList<Album> z = new ArrayList<>();
        try (ObjectInputStream objInputStream = new ObjectInputStream(
                new FileInputStream(FILENAMEALBUMES))) {
                
                    z = (ArrayList<Album>) objInputStream.readObject();
                    //System.out.println(z.toString());
               
            } catch (EOFException eof) {
                LOGGER.config("Se cargaron los Vehiculos...");
            } catch (ClassNotFoundException ex) {
                LOGGER.config("Corrupted file");
            

        } catch (FileNotFoundException e1) {
            LOGGER.config(e1.getMessage());
            Platform.exit();
        } catch (IOException e2) {
            LOGGER.config(e2.getMessage());
            Platform.exit();
        }
        return z;
    }

    public synchronized static void guardarAlbumes(ArrayList<Album> ListaALbumes) {
        try (ObjectOutputStream objOutputStream = new ObjectOutputStream(
                new FileOutputStream(FILENAMEALBUMES))) {
            objOutputStream.writeObject(ListaALbumes);
            LOGGER.config("... written to albumes.dat.");

        } catch (FileNotFoundException e1) {
            LOGGER.config(e1.getMessage());
        } catch (IOException e2) {
            LOGGER.config(e2.getMessage());
        }
    }

    public synchronized static ArrayList<Foto> cargarFotos() {
        ArrayList<Foto> z = new ArrayList<>();
        try (ObjectInputStream objInputStream = new ObjectInputStream(
                new FileInputStream(FILENAMEFOTOS))) {
                    z = (ArrayList<Foto>) objInputStream.readObject();
                    //System.out.println(z.toString());
                
        } catch (EOFException eof) {
            LOGGER.config("Se cargaron las fotos...");
        } catch (ClassNotFoundException ex) {
            LOGGER.config("Corrupted file");
        }

        catch (FileNotFoundException e1) {
            LOGGER.config(e1.getMessage());
            Platform.exit();
        } catch (IOException e2) {
            LOGGER.config(e2.getMessage());
            Platform.exit();
        }
        return z;
    }

    public synchronized static void guardarSolicitudes(ArrayList<Foto> ListaFotos) {
        try (ObjectOutputStream objOutputStream = new ObjectOutputStream(
                new FileOutputStream(FILENAMEFOTOS))) {
            objOutputStream.writeObject(ListaFotos);
            LOGGER.config("... written to fotos.dat.");

        } catch (FileNotFoundException e1) {
            LOGGER.config(e1.getMessage());
        } catch (IOException e2) {
            LOGGER.config(e2.getMessage());
        }
    }

    public synchronized static ArrayList<Reaccion> cargarReaccion() {
        ArrayList<Reaccion> z = new ArrayList<>();
        try (ObjectInputStream objInputStream = new ObjectInputStream(
                new FileInputStream(FILENAMEREACCIONES))) {
           
                z = (ArrayList<Reaccion>) objInputStream.readObject();
                    //System.out.println(z.toString());
                           
        } catch (EOFException eof) {
            LOGGER.config("Se cargaron los mantenimientos...");
        } catch (ClassNotFoundException ex) {
            LOGGER.config("Corrupted file");
        }
        catch (FileNotFoundException e1) {
            LOGGER.config(e1.getMessage());
            Platform.exit();
        } catch (IOException e2) {
            LOGGER.config(e2.getMessage());
            Platform.exit();
        }
        return z;
    }

    public synchronized static void guardarMantenimientos(ArrayList<Reaccion> ListaReacciones) {
        try (ObjectOutputStream objOutputStream = new ObjectOutputStream(
                new FileOutputStream(FILENAMEREACCIONES))) {
            objOutputStream.writeObject(ListaReacciones);
            LOGGER.config("... written to mantenimientos.dat.");

        } catch (FileNotFoundException e1) {
            LOGGER.config(e1.getMessage());
        } catch (IOException e2) {
            LOGGER.config(e2.getMessage());
        }
    }

}

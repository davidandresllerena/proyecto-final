/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Herramientas;

/**
 *
 * @author Grupo 7 (David Llerena,Ariel Velez,Pedro Conforme)
 */
public class CONSTANTES {

    public static String RUTA_IMAGENES = "file:src/Recursos/Imagenes/";
    public static String RUTA_ESTILOS = "file:src/Interfaz/";
    public static double EspacioRight = 10.0;
    public static int ANCHOPANTALLA = java.awt.Toolkit.getDefaultToolkit().getScreenSize().width;
    public static int ALTOPANTALLA = java.awt.Toolkit.getDefaultToolkit().getScreenSize().height;
    public static int ANCHOPANTALLADEFAULT = ANCHOPANTALLA - 450;
    public static int ALTOPANTALLADEFAULT = ALTOPANTALLA - 220;

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Foto;

import Modelo.Herramientas.CONSTANTES;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Grupo 7 (David Llerena,Ariel Velez,Pedro Conforme)
 */
public class Camara implements Serializable {

    private String Marca;
    private String Modelo;
    private String PathImagen;

    public Camara(String Marca, String Modelo) {
        this.Marca = Marca;
        this.Modelo = Modelo;
        this.PathImagen = CONSTANTES.RUTA_IMAGENES + "camarapre.png";
    }

    public Camara(String Marca, String Modelo, String PathImagen) {
        this.Marca = Marca;
        this.Modelo = Modelo;
        this.PathImagen = PathImagen;
    }

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String Marca) {
        this.Marca = Marca;
    }

    public String getModelo() {
        return Modelo;
    }

    public void setModelo(String Modelo) {
        this.Modelo = Modelo;
    }

    public String getPathImagen() {
        return PathImagen;
    }

    public void setPathImagen(String PathImagen) {
        this.PathImagen = PathImagen;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.Marca);
        hash = 17 * hash + Objects.hashCode(this.Modelo);
        hash = 17 * hash + Objects.hashCode(this.PathImagen);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Camara other = (Camara) obj;
        if (!Objects.equals(this.Marca, other.Marca)) {
            return false;
        }
        if (!Objects.equals(this.Modelo, other.Modelo)) {
            return false;
        }
        if (!Objects.equals(this.PathImagen, other.PathImagen)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Marca= " + Marca + ", Modelo= " + Modelo;
    }

}
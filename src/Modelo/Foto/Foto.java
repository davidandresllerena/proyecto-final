/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Foto;

import Modelo.Usuario.Usuario;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Grupo 7 (David Llerena,Ariel Velez,Pedro Conforme)
 */
public class Foto implements Serializable {

    private static int IdFotos;
    private int IdFoto;
    private String descripcion;
    private LocalDate fechaCreación;
    private String lugar;
    private ArrayList<String> HashTag;
    private ArrayList<String> Integrantes;
    private ArrayList<Integer> IDalbumes;
    private ArrayList<Reaccion> Reacciones;
    private Camara camara;
    private ArrayList<Comentario> Comentarios;
    private String PathFoto;

    public Foto(String descripcion, String lugar, Camara camara, String PathFoto) {
        this.IdFoto = IdFotos;
        IdFotos++;
        this.fechaCreación = LocalDate.now();
        HashTag = new ArrayList<>();
        Integrantes = new ArrayList<>();
        IDalbumes = new ArrayList<>();
        Reacciones = new ArrayList<>();
        Comentarios = new ArrayList<>();
        this.descripcion = descripcion;
        this.lugar = lugar;
        this.camara = camara;
        this.PathFoto = PathFoto;
    }

    public void anadirhashtag(String Hash) {
        HashTag.add(Hash);
    }

    public void anadirIntegrande(String I) {
        Integrantes.add(I);
    }

    public void anadirIdAlbum(Integer id) {
        IDalbumes.add(id);
    }

    public void anadirReaccion(Reaccion r) {
        Reacciones.add(r);
    }

    public void quitarReaccion(Reaccion r) {
        Reacciones.remove(r);
    }

    public int getIdFoto() {
        return IdFoto;
    }

    public void setIdFoto(int IdFoto) {
        this.IdFoto = IdFoto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public LocalDate getFechaCreación() {
        return fechaCreación;
    }

    public void setFechaCreación(LocalDate fechaCreación) {
        this.fechaCreación = fechaCreación;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public ArrayList<String> getHashTag() {
        return HashTag;
    }

    public void setHashTag(ArrayList<String> HashTag) {
        this.HashTag = HashTag;
    }

    public ArrayList<String> getIntegrantes() {
        return Integrantes;
    }

    public void setIntegrantes(ArrayList<String> Integrantes) {
        this.Integrantes = Integrantes;
    }

    public ArrayList<Integer> getIDalbumes() {
        return IDalbumes;
    }

    public void setIDalbumes(ArrayList<Integer> IDalbumes) {
        this.IDalbumes = IDalbumes;
    }

    public ArrayList<Reaccion> getReacciones() {
        return Reacciones;
    }

    public void setReacciones(ArrayList<Reaccion> Reacciones) {
        this.Reacciones = Reacciones;
    }

    public Camara getCamara() {
        return camara;
    }

    public void setCamara(Camara camara) {
        this.camara = camara;
    }

    public ArrayList<Comentario> getComentarios() {
        return Comentarios;
    }

    public void setComentarios(ArrayList<Comentario> Comentarios) {
        this.Comentarios = Comentarios;
    }

    public String getPathFoto() {
        return PathFoto;
    }

    public void setPathFoto(String PathFoto) {
        this.PathFoto = PathFoto;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 13 * hash + this.IdFoto;
        hash = 13 * hash + Objects.hashCode(this.descripcion);
        hash = 13 * hash + Objects.hashCode(this.fechaCreación);
        hash = 13 * hash + Objects.hashCode(this.lugar);
        hash = 13 * hash + Objects.hashCode(this.HashTag);
        hash = 13 * hash + Objects.hashCode(this.Integrantes);
        hash = 13 * hash + Objects.hashCode(this.IDalbumes);
        hash = 13 * hash + Objects.hashCode(this.Reacciones);
        hash = 13 * hash + Objects.hashCode(this.camara);
        hash = 13 * hash + Objects.hashCode(this.Comentarios);
        hash = 13 * hash + Objects.hashCode(this.PathFoto);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Foto other = (Foto) obj;
        if (this.IdFoto != other.IdFoto) {
            return false;
        }
        if (!Objects.equals(this.descripcion, other.descripcion)) {
            return false;
        }
        if (!Objects.equals(this.lugar, other.lugar)) {
            return false;
        }

        if (!Objects.equals(this.PathFoto, other.PathFoto)) {
            return false;
        }
        if (!Objects.equals(this.fechaCreación, other.fechaCreación)) {
            return false;
        }
        if (!Objects.equals(this.HashTag, other.HashTag)) {
            return false;
        }
        if (!Objects.equals(this.Integrantes, other.Integrantes)) {
            return false;
        }
        if (!Objects.equals(this.IDalbumes, other.IDalbumes)) {
            return false;
        }
        if (!Objects.equals(this.Reacciones, other.Reacciones)) {
            return false;
        }
        if (!Objects.equals(this.camara, other.camara)) {
            return false;
        }
        if (!Objects.equals(this.Comentarios, other.Comentarios)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Foto{" + "IdFoto=" + IdFoto + ", descripcion=" + descripcion + ", fechaCreaci\u00f3n=" + fechaCreación + ", lugar=" + lugar + ", HashTag=" + HashTag + ", Integrantes=" + Integrantes + ", IDalbumes=" + IDalbumes + ", Reacciones=" + Reacciones + ", camara=" + camara + ", Comentarios=" + Comentarios + ", PathFoto=" + PathFoto + '}';
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Foto;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Grupo 7 (David Llerena,Ariel Velez,Pedro Conforme)
 */
public class Reaccion implements Serializable {

    private String Nombre;
    private String Ubicacion;

    public Reaccion(String Nombre, String Ubicacion) {
        this.Nombre = Nombre;
        this.Ubicacion = Ubicacion;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getUbicacion() {
        return Ubicacion;
    }

    public void setUbicacion(String Ubicacion) {
        this.Ubicacion = Ubicacion;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.Nombre);
        hash = 53 * hash + Objects.hashCode(this.Ubicacion);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Reaccion other = (Reaccion) obj;
        if (!Objects.equals(this.Nombre, other.Nombre)) {
            return false;
        }
        if (!Objects.equals(this.Ubicacion, other.Ubicacion)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return Nombre;
    }

}

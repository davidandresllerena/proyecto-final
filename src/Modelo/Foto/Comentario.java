/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Foto;

import Modelo.Usuario.Usuario;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 *
 * @author Grupo 7 (David Llerena,Ariel Velez,Pedro Conforme)
 */
public class Comentario implements Serializable {

    private String Contenido;
    private LocalDateTime FechaComentario;
    private Usuario EmisorComentario;

    public Comentario(String Contenido, LocalDateTime FechaComentario, Usuario EmisorComentario) {
        this.Contenido = Contenido;
        this.FechaComentario = FechaComentario;
        this.EmisorComentario = EmisorComentario;
    }

    public String getContenido() {
        return Contenido;
    }

    public void setContenido(String Contenido) {
        this.Contenido = Contenido;
    }

    public LocalDateTime getFechaComentario() {
        return FechaComentario;
    }

    public void setFechaComentario(LocalDateTime FechaComentario) {
        this.FechaComentario = FechaComentario;
    }

    public Usuario getEmisorComentario() {
        return EmisorComentario;
    }

    public void setEmisorComentario(Usuario EmisorComentario) {
        this.EmisorComentario = EmisorComentario;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.Contenido);
        hash = 53 * hash + Objects.hashCode(this.FechaComentario);
        hash = 53 * hash + Objects.hashCode(this.EmisorComentario);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Comentario other = (Comentario) obj;
        if (!Objects.equals(this.Contenido, other.Contenido)) {
            return false;
        }
        if (!Objects.equals(this.FechaComentario, other.FechaComentario)) {
            return false;
        }
        if (!Objects.equals(this.EmisorComentario, other.EmisorComentario)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Comentario{" + "Contenido=" + Contenido + ", FechaComentario=" + FechaComentario + ", EmisorComentario=" + EmisorComentario + '}';
    }

}

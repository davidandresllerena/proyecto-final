/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz;

import Modelo.Foto.Foto;
import Modelo.Foto.Reaccion;
import Modelo.Herramientas.CONSTANTES;
import Modelo.ModeloGaleria.Album;
import Modelo.Sistema;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Grupo 7 (David Llerena,Pedro Conforme,Ariel Velez)
 */
public class VentanaSlideShow {

    private BorderPane rootFormulario;
    private Button Atras;
    private Button Bmu;
    private Button Play;
    private Button Pause;
    private Button PlayM;
    private Button PauseM;
    private Button SetTime;
    private String RutaMusica = "";
    private Image Mus;
    private ImageView FotoS;
    private Album album;
    private Image VisualizarFoto;
    private ImageView VisualizarFotoV;
    private boolean bandera;
    private VBox Vtotal;
    private HBox Hbotones;
    private int ContadorHilo;
    private ScrollPane PanelBarra;
    private TextField Tcamara;
    private int DuraciónCambio;
    private CambiarFoto hiloCambiarFoto;
    private Thread THCFoto;
    private HBox UltimaLinea;
    private TextField CambiarTiempo;
    private String pathMus;
    private MediaPlayer mediaPlayer;

    public VentanaSlideShow(Album album) {
        this.album = album;
        bandera = true;
        DuraciónCambio = 3000;
        rootFormulario = new BorderPane();
        Image fondoIntro = new Image(CONSTANTES.RUTA_IMAGENES + "fondoinicio.jpg", 450, 750, false, true);
        rootFormulario.setBackground(new Background(new BackgroundImage(fondoIntro, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT, new BackgroundSize(450, 750, true, true, true, true))));
        rootFormulario.setStyle("-fx-border-color: white; -fx-border-width: 5; ");
        rootFormulario.setPrefSize(850, 1000);
        rootFormulario.setTop(crearEncabezado());
        rootFormulario.setCenter(crearCuerpoF());
        hiloCambiarFoto = new CambiarFoto();
        THCFoto = new Thread(hiloCambiarFoto);
        THCFoto.start();
    }

    /**
     * Crea el encabezado de la ventana para editar una foto
     *
     * @return HBox
     */
    private HBox crearEncabezado() {

        HBox Hencabezado = new HBox();
        Label Ltop = new Label("SlideShow");
        Ltop.setTextFill(Color.web("white"));
        Ltop.setFont(Font.font("arial", FontWeight.BOLD, 20));
        Hencabezado.setAlignment(Pos.CENTER);
        Hencabezado.getChildren().addAll(Ltop);
        Hencabezado.setPrefHeight(30);
        return Hencabezado;
    }

    private ScrollPane crearCuerpoF() {
        // Creo el contenedor principal
        Vtotal = new VBox();
        Vtotal.setSpacing(35);
        Vtotal.setAlignment(Pos.CENTER);
        PanelBarra = new ScrollPane(Vtotal);
        PanelBarra.setStyle("-fx-border-color: black");
        PanelBarra.setFitToHeight(true);
        PanelBarra.setFitToWidth(true);
        PanelBarra.setStyle("-fx-background: #0BB9FF;");

        VisualizarFoto = null;
        if (album.getFotos().get(0).getPathFoto().contains("file:")) {
            VisualizarFoto = new Image(album.getFotos().get(0).getPathFoto(), 600, 600, true, true);
        } else {
            VisualizarFoto = new Image("file:" + album.getFotos().get(0).getPathFoto(), 600, 600, true, true);
        }
        VisualizarFotoV = new ImageView(VisualizarFoto);
//        bandera = true;
        //Creo el contenedo para los botones
        Hbotones = new HBox();
        Hbotones.setSpacing(20);
        Atras = new Button("Regresar");
        Image imguardar = new Image(CONSTANTES.RUTA_IMAGENES + "atras.png", 18, 18, true, true);
        ImageView imguardarv = new ImageView(imguardar);
        Atras.setGraphic(imguardarv);
        Atras.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");

        Play = new Button("Play");
        Image implay = new Image(CONSTANTES.RUTA_IMAGENES + "play.png", 18, 18, true, true);
        ImageView implayv = new ImageView(implay);
        Play.setGraphic(implayv);
        Play.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");
        Play.setOnAction(bp -> {
            bandera = true;
            hiloCambiarFoto.reanudarCambiarFoto();
            hiloCambiarFoto = new CambiarFoto();
            THCFoto = new Thread(hiloCambiarFoto);
            THCFoto.start();
        });

        Pause = new Button("Pausa");
        Image impause = new Image(CONSTANTES.RUTA_IMAGENES + "pause.png", 18, 18, true, true);
        ImageView impausev = new ImageView(impause);
        Pause.setGraphic(impausev);
        Pause.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");

        Pause.setOnAction(bp -> {
            bandera = false;
        });

        UltimaLinea = new HBox();
        UltimaLinea.setSpacing(20);
        CambiarTiempo = new TextField("");
        CambiarTiempo.setPromptText("Ingrese el tiempo en segundos.");
        CambiarTiempo.setPrefSize(180, 20);

        SetTime = new Button("Tiempo [s]");
        Image imst = new Image(CONSTANTES.RUTA_IMAGENES + "time.png", 18, 18, true, true);
        ImageView imstv = new ImageView(imst);
        SetTime.setGraphic(imstv);
        SetTime.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");

        SetTime.setOnAction(bp -> {
            if (!(CambiarTiempo.getText().equals(""))) {
                if (Integer.valueOf(CambiarTiempo.getText()) > 0) {
                    DuraciónCambio = Integer.valueOf(CambiarTiempo.getText()) * 1000;
                } else {
                    Alert dialogoAlerta = new Alert(Alert.AlertType.WARNING);
                    dialogoAlerta.setTitle("¡No se pudo cambiar el tiempo!");
                    Image imaviso = new Image(CONSTANTES.RUTA_IMAGENES + "cam.gif", 250, 250, true, true);
                    ImageView imavisov = new ImageView(imaviso);
                    dialogoAlerta.setGraphic(imavisov);
                    dialogoAlerta.setHeaderText("El intervalo de transición no pudo ser modificado");
                    dialogoAlerta.setContentText("¡Debe elegir solo un número mayor de cero!");
                    dialogoAlerta.initStyle(StageStyle.UTILITY);
                    java.awt.Toolkit.getDefaultToolkit().beep();
                    dialogoAlerta.showAndWait();
                }

            }
        });

        Bmu = new Button("Buscar Música");
        Image imbm = new Image(CONSTANTES.RUTA_IMAGENES + "find.png", 20, 20, true, true);
        ImageView imbmv = new ImageView(imbm);
        Bmu.setGraphic(imbmv);
        Bmu.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 15px;");
        Bmu.setOnAction(ebmu -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Seleccione una música");
            File defaultDirectory = new File(System.getProperty("user.home"));
            fileChooser.setInitialDirectory(defaultDirectory);
            // Agregar filtros para facilitar la busqueda
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("MP3", "*.mp3")
            );

            // Obtener la imagen seleccionada
            Stage SeleccionImagen = new Stage();
            File imgMusi = fileChooser.showOpenDialog(SeleccionImagen);

            //Mostar la imagen
            if (imgMusi != null) {
                try {
                    RutaMusica = imgMusi.getCanonicalPath();
                } catch (IOException ex) {
                    Logger.getLogger(VentanaSlideShow.class.getName()).log(Level.SEVERE, null, ex);
                }
                pathMus = RutaMusica;
                Media media = new Media(new File(pathMus).toURI().toString());
                mediaPlayer = new MediaPlayer(media);
                mediaPlayer.setAutoPlay(true);
                MediaView mediaView = new MediaView(mediaPlayer);
            }

        });

        PlayM = new Button("Play Music");
        Image implayM = new Image(CONSTANTES.RUTA_IMAGENES + "play.png", 18, 18, true, true);
        ImageView implayMv = new ImageView(implayM);
        PlayM.setGraphic(implayMv);
        PlayM.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");
        PlayM.setOnAction(bp -> {
            mediaPlayer.play();
        });

        PauseM = new Button("Stop Music");
        Image impauseM = new Image(CONSTANTES.RUTA_IMAGENES + "pause.png", 18, 18, true, true);
        ImageView impauseMv = new ImageView(impauseM);
        PauseM.setGraphic(impauseMv);
        PauseM.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");

        PauseM.setOnAction(bp -> {
            mediaPlayer.pause();
        });

        UltimaLinea.setAlignment(Pos.CENTER);
        UltimaLinea.getChildren().addAll(CambiarTiempo, Bmu, PlayM, PauseM);
        Hbotones.setAlignment(Pos.CENTER);
        Hbotones.getChildren().addAll(Play, Pause, SetTime, Atras);
        Vtotal.getChildren().addAll(VisualizarFotoV, Hbotones, UltimaLinea);
        //Agrego todos los contenedor al conetenedor principal
        return PanelBarra;
    }

    private class CambiarFoto implements Runnable {

        @Override
        public void run() {
            while (bandera) {
                try {
                    if (ContadorHilo < album.getFotos().size()) {
//                        Visor Foto
                        VisualizarFoto = null;
                        if (album.getFotos().get(ContadorHilo).getPathFoto().contains("file:")) {
                            VisualizarFoto = new Image(album.getFotos().get(ContadorHilo).getPathFoto(), 600, 600, true, true);
                        } else {
                            VisualizarFoto = new Image("file:" + album.getFotos().get(ContadorHilo).getPathFoto(), 600, 600, true, true);
                        }
                        VisualizarFotoV = new ImageView(VisualizarFoto);

                    }
                    if (ContadorHilo == album.getFotos().size()) {
                        ContadorHilo = 0;
                    }

                    Thread.sleep(DuraciónCambio);
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            ContadorHilo += 1;
                            Vtotal.getChildren().clear();
                            Vtotal.getChildren().addAll(VisualizarFotoV, Hbotones, UltimaLinea);
                        }
                    });

                } catch (InterruptedException ex) {
                    Logger.getLogger(VentanaSlideShow.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        public void pararCambiarFoto() {
            bandera = false;
        }

        public void reanudarCambiarFoto() {
            bandera = true;
        }
    }

    public BorderPane getRootFormulario() {
        return rootFormulario;
    }

    public void setRootFormulario(BorderPane rootFormulario) {
        this.rootFormulario = rootFormulario;
    }

    public Button getAtras() {
        return Atras;
    }

    public void setAtras(Button Atras) {
        this.Atras = Atras;
    }

    public Button getBfoto() {
        return Bmu;
    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    public void setBfoto(Button Bfoto) {
        this.Bmu = Bfoto;
    }

    public String getRutaFoto() {
        return RutaMusica;
    }

    public void setRutaFoto(String RutaFoto) {
        this.RutaMusica = RutaFoto;
    }

    public Image getPortadaAlbum() {
        return Mus;
    }

    public void setPortadaAlbum(Image PortadaAlbum) {
        this.Mus = PortadaAlbum;
    }

    public ImageView getFotoS() {
        return FotoS;
    }

    public void setFotoS(ImageView FotoS) {
        this.FotoS = FotoS;
    }

    public boolean isBandera() {
        return bandera;
    }

    public void setBandera(boolean bandera) {
        this.bandera = bandera;
    }

    public int getContadorHilo() {
        return ContadorHilo;
    }

    public void setContadorHilo(int ContadorHilo) {
        this.ContadorHilo = ContadorHilo;
    }

    public int getDuraciónCambio() {
        return DuraciónCambio;
    }

    public void setDuraciónCambio(int DuraciónCambio) {
        this.DuraciónCambio = DuraciónCambio;
    }

}

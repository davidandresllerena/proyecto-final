/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz;

import Modelo.Herramientas.CONSTANTES;
import Modelo.ModeloGaleria.Album;
import Modelo.Sistema;
import java.io.File;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author Grupo 7 (David Llerena,Pedro Conforme,Ariel Velez)
 */
public class VentanaNuevoAlbum {

    private BorderPane rootFormulario;
    private Button Guardar;
    private Button Cancelar;
    private Button Bfoto;
    private Button FSel;
    private TextField Tnombre;
    private TextField TDescrip;
    private FileChooser SelecFoto;
    private String RutaFoto = "";
    private Image PortadaAlbum;
    private ImageView FotoS;

    public VentanaNuevoAlbum() {

        rootFormulario = new BorderPane();
        Image fondoIntro = new Image(CONSTANTES.RUTA_IMAGENES + "fondoinicio.jpg", 450, 750, false, true);
        rootFormulario.setBackground(new Background(new BackgroundImage(fondoIntro, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT, new BackgroundSize(450, 750, true, true, true, true))));
        rootFormulario.setStyle("-fx-border-color: white; -fx-border-width: 5; ");
        rootFormulario.setPrefSize(450, 750);
        rootFormulario.setTop(crearEncabezado());
        rootFormulario.setCenter(crearCuerpoF());
    }

    /**
     * Crea el encabezado de la ventana para crear un album
     *
     * @return HBox
     */
    private HBox crearEncabezado() {

        HBox Hencabezado = new HBox();
        Label Ltop = new Label("Crear un nuevo album");
        Ltop.setTextFill(Color.web("white"));
        Ltop.setFont(Font.font("arial", FontWeight.BOLD, 20));
        Hencabezado.setAlignment(Pos.CENTER);
        Hencabezado.getChildren().addAll(Ltop);
        Hencabezado.setPrefHeight(30);
        return Hencabezado;
    }

    /**
     * crea el contenido centrar de la ventana de creacion de un album
     *
     * @return VBox
     */
    @SuppressWarnings("empty-statement")
    private VBox crearCuerpoF() {
        // Creo el contenedor principal
        VBox Vtotal = new VBox();
        Vtotal.setSpacing(35);
        Vtotal.setAlignment(Pos.CENTER);
//        Vtotal.setStyle("-fx-background-color: #00FFFF");

        // Creo el contenedor para el nombre
        AnchorPane Hnombre = new AnchorPane();
        Label Lnombre = new Label("Nombre: ");
        Lnombre.setFont(Font.font("arial", FontWeight.BOLD, 18));
        Tnombre = new TextField("");
        Tnombre.setPromptText("Ingrese el nombre del album");
        Tnombre.setPrefSize(300, 10);
        Hnombre.getChildren().addAll(Lnombre, Tnombre);

        // Creo el contenedor para la descripcion
        AnchorPane Hdescp = new AnchorPane();
        Label Ldescp = new Label("Descripcion: ");
        Ldescp.setFont(Font.font("arial", FontWeight.BOLD, 18));
        TDescrip = new TextField("");
        TDescrip.setPromptText("Ingrese la descripcion album");
        TDescrip.setPrefSize(300, 10);
        Hdescp.getChildren().addAll(Ldescp, TDescrip);

        // Creo el contenedor para el descuento
        AnchorPane Hfoto = new AnchorPane();
        AnchorPane Hvisual = new AnchorPane();
        Label Lfoto = new Label("Portada: ");
        Lfoto.setFont(Font.font("arial", FontWeight.BOLD, 18));
        Bfoto = new Button("Buscar Portada Album");
        Image isf = new Image(CONSTANTES.RUTA_IMAGENES + "loadimage.png", 20, 20, true, true);
        ImageView isfv = new ImageView(isf);
        Bfoto.setGraphic(isfv);
        Bfoto.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 15px;");
        FSel = new Button();
        FSel.setStyle("-fx-background-color: #ffffff;");
        FSel.setGraphic(new ImageView(PortadaAlbum));
        Bfoto.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Seleccione una Imagen");
            File defaultDirectory = new File(System.getProperty("user.home"));
            fileChooser.setInitialDirectory(defaultDirectory);
            // Agregar filtros para facilitar la busqueda
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("All Images", "*.*"),
                    new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                    new FileChooser.ExtensionFilter("PNG", "*.png")
            );

            // Obtener la imagen seleccionada
            Stage SeleccionImagen = new Stage();
            File imgFile = fileChooser.showOpenDialog(SeleccionImagen);

            //Mostar la imagen
            if (imgFile != null) {
                PortadaAlbum = new Image("file:" + imgFile.getAbsolutePath(), 150, 150, true, true);
                RutaFoto = imgFile.getAbsolutePath();
                FSel.setGraphic(new ImageView(PortadaAlbum));
                FSel.setVisible(true);
                ;
            }
        });
        Hfoto.getChildren().addAll(Lfoto, Bfoto);
        Hvisual.getChildren().addAll(FSel);

        // Creo el contenedo para los botones
        HBox Hbotones = new HBox();
        Hbotones.setSpacing(20);
        Guardar = new Button("Guardar Edicion");
        Image imguardar = new Image(CONSTANTES.RUTA_IMAGENES + "save.png", 18, 18, true, true);
        ImageView imguardarv = new ImageView(imguardar);
        Guardar.setGraphic(imguardarv);
        Guardar.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");
        Cancelar = new Button("Cancelar Edicion");
        Image imcancelar = new Image(CONSTANTES.RUTA_IMAGENES + "cancel.png", 18, 18, true, true);
        ImageView imcancelarv = new ImageView(imcancelar);
        Cancelar.setGraphic(imcancelarv);
        Cancelar.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");
        Hbotones.setAlignment(Pos.CENTER);
        Hbotones.getChildren().addAll(Guardar, Cancelar);

        //Agrego todos los contenedor al conetenedor principal
        Vtotal.getChildren().addAll(Hnombre, Hdescp, Hfoto, Hvisual, Hbotones);
//        Les asigno la posicion a acada label y textbox
        AnchorPane.setRightAnchor(Tnombre, CONSTANTES.EspacioRight);
        AnchorPane.setRightAnchor(TDescrip, CONSTANTES.EspacioRight);
        AnchorPane.setRightAnchor(FSel, CONSTANTES.EspacioRight * 2);
        AnchorPane.setLeftAnchor(Bfoto, CONSTANTES.EspacioRight * 15);
        AnchorPane.setRightAnchor(FSel, CONSTANTES.EspacioRight * 18);
        AnchorPane.setLeftAnchor(Lnombre, CONSTANTES.EspacioRight);
        AnchorPane.setLeftAnchor(Ldescp, CONSTANTES.EspacioRight);
        AnchorPane.setLeftAnchor(Lfoto, CONSTANTES.EspacioRight);
        return Vtotal;
    }

    public TextField getTDescrip() {
        return TDescrip;
    }

    public void setTDescrip(TextField TDescrip) {
        this.TDescrip = TDescrip;
    }

    public BorderPane getRootFormulario() {
        return rootFormulario;
    }

    public Button getGuardar() {
        return Guardar;
    }

    public Button getCancelar() {
        return Cancelar;
    }

    public TextField getTnombre() {
        return Tnombre;
    }

    public String getRutaFoto() {
        return RutaFoto;
    }

    public Button getBfoto() {
        return Bfoto;
    }

    public void setBfoto(Button Bfoto) {
        this.Bfoto = Bfoto;
    }

    public Button getFSel() {
        return FSel;
    }

    public void setFSel(Button FSel) {
        this.FSel = FSel;
    }

    public FileChooser getSelecFoto() {
        return SelecFoto;
    }

    public void setSelecFoto(FileChooser SelecFoto) {
        this.SelecFoto = SelecFoto;
    }

    public Image getImagenConductor() {
        return PortadaAlbum;
    }

    public void setImagenConductor(Image imagenConductor) {
        this.PortadaAlbum = imagenConductor;
    }

    public ImageView getFotoS() {
        return FotoS;
    }

    public void setFotoS(ImageView FotoS) {
        this.FotoS = FotoS;
    }

    public Image getPortadaAlbum() {
        return PortadaAlbum;
    }

    public void setPortadaAlbum(Image PortadaAlbum) {
        this.PortadaAlbum = PortadaAlbum;
    }

}

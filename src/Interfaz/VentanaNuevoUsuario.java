/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz;

import Modelo.Herramientas.CONSTANTES;
import Modelo.Sistema;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author Grupo 7 (David Llerena,Pedro Conforme,Ariel Velez)
 */
public class VentanaNuevoUsuario {

    private BorderPane rootFormulario;
    private Button Guardar;
    private Button Cancelar;
    private TextField Tnombre;
    private TextField Tcorreo;
    private TextField TContra;
    private TextField TAlias;

    public VentanaNuevoUsuario() {
        rootFormulario = new BorderPane();
        Image fondoIntro = new Image(CONSTANTES.RUTA_IMAGENES + "fondoinicio.jpg", 450, 750, false, true);
        rootFormulario.setBackground(new Background(new BackgroundImage(fondoIntro, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT, new BackgroundSize(450, 750, true, true, true, true))));
        rootFormulario.setStyle("-fx-border-color: white; -fx-border-width: 5; ");
        rootFormulario.setPrefSize(450, 750);
        rootFormulario.setTop(crearEncabezado());
        rootFormulario.setCenter(crearCuerpoF());
    }

    /**
     * Crea el encabezado de la ventana nuevo usuario
     *
     * @return HBox
     */
    private HBox crearEncabezado() {

        HBox Hencabezado = new HBox();
        Label Ltop = new Label("Crear un Nuevo Usuario");
        Ltop.setTextFill(Color.web("white"));
        Ltop.setFont(Font.font("arial", FontWeight.BOLD, 20));
        Hencabezado.setAlignment(Pos.CENTER);
        Hencabezado.getChildren().addAll(Ltop);
        Hencabezado.setPrefHeight(30);
        return Hencabezado;
    }

    /**
     * Crea el contenido central de la ventana nuevo usuario
     *
     * @return VBox
     */
    private VBox crearCuerpoF() {
        // Creo el contenedor principal
        VBox Vtotal = new VBox();
        Vtotal.setSpacing(35);
        Vtotal.setAlignment(Pos.CENTER);
//        Vtotal.setStyle("-fx-background-color: #00FFFF");

        // Creo el contenedor para el nombre
        AnchorPane Hnombre = new AnchorPane();
        Label Lnombre = new Label("Nombre: ");
        Lnombre.setFont(Font.font("arial", FontWeight.BOLD, 18));
        Tnombre = new TextField("");
        Tnombre.setPromptText("Ingrese el nombre del usuario.");
        Tnombre.setPrefSize(300, 10);
        Hnombre.getChildren().addAll(Lnombre, Tnombre);

        // Creo el contenedor para el precio
        AnchorPane HCorreo = new AnchorPane();
        Label Lcorreo = new Label("Correo: ");
        Lcorreo.setFont(Font.font("arial", FontWeight.BOLD, 18));
        Tcorreo = new TextField();
        Tcorreo.setPrefSize(300, 10);
        Tcorreo.setPromptText("Ingrese el correo del usuario.");
        HCorreo.getChildren().addAll(Lcorreo, Tcorreo);

        AnchorPane HContrasena = new AnchorPane();
        Label Lcontra = new Label("Contraseña: ");
        Lcontra.setFont(Font.font("arial", FontWeight.BOLD, 18));
        TContra = new TextField();
        TContra.setPromptText("Ingrese la contraseña del usuario.");
        TContra.setPrefSize(300, 10);
        HContrasena.getChildren().addAll(Lcontra, TContra);

        AnchorPane HAlias = new AnchorPane();
        Label LAlias = new Label("Alias: ");
        LAlias.setFont(Font.font("arial", FontWeight.BOLD, 18));
        TAlias = new TextField();
        TAlias.setPromptText("Ingrese el sobrenombre del usuario.");
        TAlias.setPrefSize(300, 10);
        HAlias.getChildren().addAll(LAlias, TAlias);

        // Creo el contenedo para los botones
        HBox Hbotones = new HBox();
        Hbotones.setSpacing(20);
        Guardar = new Button("Guardar");
        Image imguardar = new Image(CONSTANTES.RUTA_IMAGENES + "save.png", 18, 18, true, true);
        ImageView imguardarv = new ImageView(imguardar);
        Guardar.setGraphic(imguardarv);
        Guardar.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");
        Cancelar = new Button("Cancelar");
        Image imcancelar = new Image(CONSTANTES.RUTA_IMAGENES + "cancel.png", 18, 18, true, true);
        ImageView imcancelarv = new ImageView(imcancelar);
        Cancelar.setGraphic(imcancelarv);
        Cancelar.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");
        Hbotones.setAlignment(Pos.CENTER);
        Hbotones.getChildren().addAll(Guardar, Cancelar);

        //Agrego todos los contenedor al conetenedor principal
        Vtotal.getChildren().addAll(Hnombre, HCorreo, HContrasena, HAlias, Hbotones);
//        Les asigno la posicion a acada label y textbox
        AnchorPane.setRightAnchor(Tnombre, CONSTANTES.EspacioRight);
        AnchorPane.setRightAnchor(Tcorreo, CONSTANTES.EspacioRight);
        AnchorPane.setRightAnchor(TContra, CONSTANTES.EspacioRight);
        AnchorPane.setRightAnchor(TAlias, CONSTANTES.EspacioRight);
        AnchorPane.setLeftAnchor(LAlias, CONSTANTES.EspacioRight);
        AnchorPane.setLeftAnchor(Lnombre, CONSTANTES.EspacioRight);
        AnchorPane.setLeftAnchor(Lcorreo, CONSTANTES.EspacioRight);
        AnchorPane.setLeftAnchor(Lcontra, CONSTANTES.EspacioRight);
        return Vtotal;
    }

    public BorderPane getRootFormulario() {
        return rootFormulario;
    }

    public void setRootFormulario(BorderPane rootFormulario) {
        this.rootFormulario = rootFormulario;
    }

    public Button getGuardar() {
        return Guardar;
    }

    public void setGuardar(Button Guardar) {
        this.Guardar = Guardar;
    }

    public Button getCancelar() {
        return Cancelar;
    }

    public void setCancelar(Button Cancelar) {
        this.Cancelar = Cancelar;
    }

    public TextField getTnombre() {
        return Tnombre;
    }

    public void setTnombre(TextField Tnombre) {
        this.Tnombre = Tnombre;
    }

    public TextField getTcorreo() {
        return Tcorreo;
    }

    public void setTcorreo(TextField Tcorreo) {
        this.Tcorreo = Tcorreo;
    }

    public TextField getTContra() {
        return TContra;
    }

    public void setTContra(TextField TContra) {
        this.TContra = TContra;
    }

    public TextField getTAlias() {
        return TAlias;
    }

    public void setTAlias(TextField TAlias) {
        this.TAlias = TAlias;
    }

}

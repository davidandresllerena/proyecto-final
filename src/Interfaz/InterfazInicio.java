package Interfaz;

import Modelo.Herramientas.CONSTANTES;
import Modelo.Herramientas.ManejoDeArchivos;
import Modelo.ModeloGaleria.Album;
import Modelo.Sistema;
import Modelo.Usuario.Usuario;
import Presentacion.ProyectoGrupo7;
import java.awt.Graphics;
import java.text.ParseException;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.management.Notification;
import javax.swing.JOptionPane;

/**
 *
 * @author
 */
public class InterfazInicio {

    private Button Login;
    private Button Exit;
    private Button Register;
    private BorderPane root;
    private TextField Tusuario;
    private TextField Tpass;
    private Scene sceneChofer;
    private Stage stageChofer;
    private VBox Htotal;
    private Scene sceneAdministrador;
    private Stage stageAdministrador;
    private boolean BanderaAsignacionPaquetes;
    private Thread Tasignacion;

    public InterfazInicio() throws ParseException {
        root = new BorderPane();
        Image fondoIntro = new Image(CONSTANTES.RUTA_IMAGENES + "fondoinicio.jpg", CONSTANTES.ANCHOPANTALLADEFAULT, CONSTANTES.ALTOPANTALLADEFAULT, false, true);
        root.setBackground(new Background(new BackgroundImage(fondoIntro, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT, new BackgroundSize(800, 600, true, true, true, true))));
        root.setPrefSize(CONSTANTES.ANCHOPANTALLADEFAULT, CONSTANTES.ALTOPANTALLADEFAULT);
        root.setCenter(crearCuerpoF());
        root.setTop(titulo());
        root.setBottom(crearBotones());
        accionesBotones();

//
    }

    /**
     * Crea el titulo de la interfaz inicial
     *
     * @return VBox
     */
    public VBox titulo() {
        VBox vTitulo = new VBox();
        Label encabezado = new Label(); // Se crea el titulo como Label(un nodo)
        encabezado.setText("LOGIN");
        encabezado.setFont(Font.font("constantia", FontWeight.BOLD, 46)); //Modifico el tipo de letra
        encabezado.setStyle("-fx-text-fill:WHITE");
        vTitulo.setAlignment(Pos.CENTER);
        vTitulo.getChildren().addAll(encabezado);
        return vTitulo;
    }

    /**
     * Crea el cuerpo de la ventana inicial
     *
     * @return VBox
     */
    private VBox crearCuerpoF() {
        Htotal = new VBox();
        Htotal.setSpacing(30);
        Htotal.setAlignment(Pos.CENTER);
        Htotal.setStyle("-fx-background-color: #FFFFFF");
        Htotal.setMaxSize(400, 400);
        HBox hUsuario = new HBox();
        hUsuario.setPrefSize(200, 60);
        Label lUsuario = new Label("Usuario: ");
        lUsuario.setFont(Font.font("arial", FontWeight.BOLD, 18));
        Tusuario = new TextField("");
        Tusuario.setPromptText("Ingrese su usuario.");
        Tusuario.setPrefSize(150, 10);
        hUsuario.setAlignment(Pos.CENTER);
        hUsuario.getChildren().addAll(lUsuario, Tusuario);

        HBox hPass = new HBox();
        hPass.setPrefSize(200, 60);
        Label lPass = new Label("Contraseña: ");
        lPass.setFont(Font.font("arial", FontWeight.BOLD, 18));
        Tpass = new TextField();
        Tpass.setPrefSize(150, 10);
        Tpass.setPromptText("Ingrese su contraseña.");
        hPass.setAlignment(Pos.CENTER);
        hPass.getChildren().addAll(lPass, Tpass);

        Button logo = new Button();
        Image imL = new Image(CONSTANTES.RUTA_IMAGENES + "inicio.gif", 300, 300, true, true);
        ImageView imLv = new ImageView(imL);
        logo.setBackground(Background.EMPTY);
        logo.setGraphic(imLv);

        Label lEmpresa = new Label("Photo-Gallery");
        lEmpresa.setFont(Font.font("Harlow Solid Italic", FontWeight.BOLD, 50));

        Htotal.getChildren().addAll(lEmpresa, logo, hUsuario, hPass);

        return Htotal;

    }

    /**
     * Crea los botones de la ventana inicial
     *
     * @return HBox
     */
    public HBox crearBotones() {

        HBox hBotones = new HBox();
        hBotones.setSpacing(20);
        Login = new Button("Entrar");
        Image imguardar = new Image(CONSTANTES.RUTA_IMAGENES + "login.png", 18, 18, true, true);
        ImageView imguardarv = new ImageView(imguardar);
        Login.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");
        Login.setGraphic(imguardarv);

        Register = new Button("Registrar");
        Image imregister = new Image(CONSTANTES.RUTA_IMAGENES + "adduser.png", 18, 18, true, true);
        ImageView imregisterv = new ImageView(imregister);
        Register.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");
        Register.setGraphic(imregisterv);

        Exit = new Button("Cancelar");
        Image imcancelar = new Image(CONSTANTES.RUTA_IMAGENES + "exit.png", 18, 18, true, true);
        ImageView imcancelarv = new ImageView(imcancelar);
        Exit.setGraphic(imcancelarv);
        Exit.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");
        hBotones.setAlignment(Pos.CENTER);
        hBotones.getChildren().addAll(Login, Register, Exit);

        return hBotones;
    }

    /**
     * Asigna las acciones a los botones de la ventana inicial
     */
    public void accionesBotones() {
        Exit.setOnAction(eSalir -> {
            Platform.exit();
        });
        HBox hb2 = new HBox();
        Register.setOnAction(eRegister -> {
            VentanaNuevoUsuario ventanaNuevoU = new VentanaNuevoUsuario();
            Scene scene1 = new Scene(ventanaNuevoU.getRootFormulario(), 450, 700);
            Stage nuevaVentana = new Stage();
            nuevaVentana.initStyle(StageStyle.TRANSPARENT);
            nuevaVentana.initModality(Modality.APPLICATION_MODAL);
            nuevaVentana.setResizable(false);
            nuevaVentana.setTitle("Nuevo Usuario");
            nuevaVentana.getIcons().add(new Image(CONSTANTES.RUTA_IMAGENES + "logo.png"));
            nuevaVentana.setScene(scene1);
            nuevaVentana.show();
            ventanaNuevoU.getCancelar().setOnAction(e3 -> {
                nuevaVentana.close();
            });
            ventanaNuevoU.getGuardar().setOnAction((ActionEvent eGN) -> {
                if (!(ventanaNuevoU.getTnombre().getText().equals("")) && !(ventanaNuevoU.getTcorreo().getText().equals("")) && !(ventanaNuevoU.getTContra().getText().equals("")) && !(ventanaNuevoU.getTAlias().getText().equals(""))) {
                    Usuario nuevoUsuario = new Usuario(ventanaNuevoU.getTnombre().getText(), ventanaNuevoU.getTcorreo().getText(), ventanaNuevoU.getTContra().getText(), ventanaNuevoU.getTAlias().getText());
                    Sistema.ListaUsuarios.add(nuevoUsuario);
                    ManejoDeArchivos.guardarUsuarios(Sistema.ListaUsuarios);
                    nuevaVentana.close();
                } else {
                    Alert dialogoAlerta = new Alert(Alert.AlertType.WARNING);
                    dialogoAlerta.setTitle("¡Error Al Crear Usuario!");
                    Image imaviso = new Image(CONSTANTES.RUTA_IMAGENES + "cam.gif", 250, 250, true, true);
                    ImageView imavisov = new ImageView(imaviso);
                    dialogoAlerta.setGraphic(imavisov);
                    dialogoAlerta.setHeaderText("No se pudo crear el nuevo usuario en el sistema");
                    dialogoAlerta.setContentText("¡Debe llenar todos los campos del formulario!");
                    dialogoAlerta.initStyle(StageStyle.UTILITY);
                    java.awt.Toolkit.getDefaultToolkit().beep();
                    dialogoAlerta.showAndWait();
                }
            });
        });
    validaLogin();
        

    }
public void validaLogin(){
    HBox hb = new HBox();
        Login.setOnAction((eventoLogin) -> {
            hb.getChildren().clear();
            Htotal.getChildren().remove(hb);
            if (Tusuario.getText().equals("") || Tpass.getText().equals("")) {
                Label mensaje = new Label("LLENE TODOS LOS CAMPOS!...");
                mensaje.setFont(Font.font("arial", FontWeight.BOLD, 18));
                hb.getChildren().add(mensaje);
                Htotal.getChildren().add(hb);
            } else {

                for (Usuario u : Sistema.ListaUsuarios) {
                    if (Tusuario.getText().equals(u.getCorreo()) && Tpass.getText().equals(u.getContraseña())) {
                        InterfazUsuario ventanaUser = new InterfazUsuario(u);
                        ProyectoGrupo7.Ini.setRoot(ventanaUser.getRoot());
                    }
                }
                Label mensajeC = new Label("CREDENCIALES INVALIDAS!...");
                mensajeC.setFont(Font.font("arial", FontWeight.BOLD, 18));
                hb.getChildren().add(mensajeC);
                Htotal.getChildren().add(hb);

            }

        });
}
    public BorderPane getRoot() {
        return root;
    }

}

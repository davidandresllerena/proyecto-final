/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz;

import Modelo.Foto.Camara;
import Modelo.Foto.Foto;
import Modelo.Herramientas.CONSTANTES;
import Modelo.ModeloGaleria.Album;
import Modelo.Sistema;
import Modelo.Usuario.Usuario;
import java.io.File;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author Grupo 7 (David Llerena,Pedro Conforme,Ariel Velez)
 */
public class VentanaEditarFoto {

    private BorderPane rootFormulario;
    private Button Atras;
    private Button Bfoto;
    private String RutaFoto = "";
    private Image PortadaAlbum;
    private ImageView FotoS;
    private Foto foto;
    private Image VisualizarFoto;
    private ImageView VisualizarFotoV;
    private ImageView VerReaccionR1V;
    private ImageView VerReaccionR2V;
    private ImageView VerReaccionR3V;
    private ImageView VerReaccionR4V;
    private ImageView VerReaccionR5V;
    private ImageView VerReaccionR6V;
    private Image VerReaccionR1;
    private Image VerReaccionR2;
    private Image VerReaccionR3;
    private Image VerReaccionR4;
    private Image VerReaccionR5;
    private Image VerReaccionR6;
    private ScrollPane PanelBarra;
    private TextField Tcamara;
    private Button Guardar;
    private Button Cancelar;
    private TextField LDatosHash;
    private TextField LdatosInte;
    private TextField LDatosdescr;
    private TextField LDatoslUGAR;
    private ComboBox CDatoscamara;
    private Usuario user;

    public VentanaEditarFoto(Foto foto, Usuario user) {
        this.user = user;
        this.foto = foto;
        rootFormulario = new BorderPane();
        Image fondoIntro = new Image(CONSTANTES.RUTA_IMAGENES + "fondoinicio.jpg", 450, 750, false, true);
        rootFormulario.setBackground(new Background(new BackgroundImage(fondoIntro, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT, new BackgroundSize(450, 750, true, true, true, true))));
        rootFormulario.setStyle("-fx-border-color: white; -fx-border-width: 5; ");
        rootFormulario.setPrefSize(850, 1000);
        rootFormulario.setTop(crearEncabezado());
        rootFormulario.setCenter(crearCuerpoF());
    }

    /**
     * Crea el encabezado de la ventana para editar una foto
     *
     * @return HBox
     */
    private HBox crearEncabezado() {

        HBox Hencabezado = new HBox();
        Label Ltop = new Label("Editor de foto");
        Ltop.setTextFill(Color.web("white"));
        Ltop.setFont(Font.font("arial", FontWeight.BOLD, 20));
        Hencabezado.setAlignment(Pos.CENTER);
        Hencabezado.getChildren().addAll(Ltop);
        Hencabezado.setPrefHeight(30);
        return Hencabezado;
    }

    public HBox validaHastag(){
        //Creo el contenedor para la descripcion
        HBox Hashtags = new HBox();
        Hashtags.setSpacing(20);
        Hashtags.setStyle("-fx-background-color: #ffffff;");
        Label LHash = new Label("HashTags(Separelos por coma): ");
        LHash.setFont(Font.font("arial", FontWeight.EXTRA_BOLD, 20));

        LDatosHash = new TextField(foto.getHashTag().toString());
        LDatosHash.setPrefSize(300, 10);
        LDatosHash.setFont(Font.font("arial", FontWeight.BOLD, 18));
        Hashtags.getChildren().addAll(LHash, LDatosHash);
        return Hashtags; 
    }
    
    public HBox validaHDescripcion(){
        //Creo el contenedor para la descripcion
        HBox Hdescripcion = new HBox();
        Hdescripcion.setSpacing(20);
        Hdescripcion.setStyle("-fx-background-color: #ffffff;");
        Label Ldescr = new Label("Descripcion: ");
        Ldescr.setFont(Font.font("arial", FontWeight.EXTRA_BOLD, 20));
        LDatosdescr = new TextField(foto.getDescripcion());
        LDatosdescr.setFont(Font.font("arial", FontWeight.BOLD, 18));
        Hdescripcion.getChildren().addAll(Ldescr, LDatosdescr);
        return Hdescripcion;
    }
    
    public HBox validaHIntegrantes(){
        //Creo el contenedor para la descripcion
        HBox Hintegrantes = new HBox();
        Hintegrantes.setSpacing(20);
        Hintegrantes.setStyle("-fx-background-color: #ffffff;");
        Label Lintegra = new Label("Integrantes (Separe los Nombres por coma): ");
        Lintegra.setFont(Font.font("arial", FontWeight.EXTRA_BOLD, 20));

        LdatosInte = new TextField(foto.getIntegrantes().toString());
        LdatosInte.setFont(Font.font("arial", FontWeight.BOLD, 18));
        Hintegrantes.getChildren().addAll(Lintegra, LdatosInte);
        return Hintegrantes;
    }
    
    public HBox validaHLugar(){
        HBox HLugar = new HBox();
        HLugar.setSpacing(20);
        HLugar.setStyle("-fx-background-color: #ffffff;");
        Label LLugar = new Label("Lugar: ");
        LLugar.setFont(Font.font("arial", FontWeight.EXTRA_BOLD, 20));
        LDatoslUGAR = new TextField(foto.getLugar());
        LDatoslUGAR.setFont(Font.font("arial", FontWeight.BOLD, 18));
        HLugar.getChildren().addAll(LLugar, LDatoslUGAR);
        return HLugar;
    }
    public HBox validaHCamara(){
        HBox Hcamara = new HBox();
        Hcamara.setSpacing(20);
        Hcamara.setStyle("-fx-background-color: #ffffff;");
        Label Lcamara = new Label("Camara: ");
        Lcamara.setFont(Font.font("arial", FontWeight.EXTRA_BOLD, 20));

        ObservableList<Camara> OLCamaras = FXCollections.observableArrayList(user.getCamaras());

        CDatoscamara = new ComboBox(OLCamaras);
        CDatoscamara.setPromptText("Seleccione una camara");
        CDatoscamara.setStyle("-fx-text-fill: WHITE; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 15px;");

//        Label CDatoscamara = new Label(foto.getCamara().toString());
//        CDatoscamara.setFont(Font.font("arial", FontWeight.BOLD, 18));
        Hcamara.getChildren().addAll(Lcamara, CDatoscamara);
        return Hcamara;
    }
    
    public HBox validaHBotones(){
        HBox Hbotones = new HBox();
        Hbotones.setSpacing(20);
        Atras = new Button("Regresar");
        Image imatras = new Image(CONSTANTES.RUTA_IMAGENES + "atras.png", 18, 18, true, true);
        ImageView imatrasv = new ImageView(imatras);
        Atras.setGraphic(imatrasv);
        Atras.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");

        Guardar = new Button("Guardar Edicion");
        Image imguardar = new Image(CONSTANTES.RUTA_IMAGENES + "save.png", 18, 18, true, true);
        ImageView imguardarv = new ImageView(imguardar);
        Guardar.setGraphic(imguardarv);
        Guardar.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");
        Cancelar = new Button("Cancelar Edicion");
        Image imcancelar = new Image(CONSTANTES.RUTA_IMAGENES + "cancel.png", 18, 18, true, true);
        ImageView imcancelarv = new ImageView(imcancelar);
        Cancelar.setGraphic(imcancelarv);
        Cancelar.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");
        Hbotones.setAlignment(Pos.CENTER);
        Hbotones.getChildren().addAll();

        Hbotones.setAlignment(Pos.CENTER);
        Hbotones.getChildren().addAll(Atras, Guardar);
        return Hbotones;
    }
    
    private ImageView validaReaccionR1V(){
        VerReaccionR1V = new ImageView(VerReaccionR1);
        ImageView VerReaccionR1VV = new ImageView(VerReaccionR1);
        if (foto.getReacciones().contains(Sistema.ListaReacciones.get(0))) {
            ReaccionesFoto.getChildren().addAll(VerReaccionR1VV);
        }
        VerReaccionR1V.setOnMouseClicked(RV1 -> {
            if (!foto.getReacciones().contains(Sistema.ListaReacciones.get(0))) {
                foto.anadirReaccion(Sistema.ListaReacciones.get(0));
                ReaccionesFoto.getChildren().add(VerReaccionR1VV);
            } else {
                foto.quitarReaccion(Sistema.ListaReacciones.get(0));
                ReaccionesFoto.getChildren().remove(VerReaccionR1VV);
            }
        });
        return VerReaccionR1V;
    }
    
    private ImageView validaReaccionR2V(){
        VerReaccionR2V = new ImageView(VerReaccionR2);
        ImageView VerReaccionR2VV = new ImageView(VerReaccionR2);
        if (foto.getReacciones().contains(Sistema.ListaReacciones.get(1))) {
            ReaccionesFoto.getChildren().addAll(VerReaccionR2VV);
        }
        VerReaccionR2V.setOnMouseClicked(RV2 -> {
            if (!foto.getReacciones().contains(Sistema.ListaReacciones.get(1))) {
                foto.anadirReaccion(Sistema.ListaReacciones.get(1));
                ReaccionesFoto.getChildren().add(VerReaccionR2VV);
            } else {
                foto.quitarReaccion(Sistema.ListaReacciones.get(1));
                ReaccionesFoto.getChildren().remove(VerReaccionR2VV);
            }
        });
        return VerReaccionR2V;
    }
     private ImageView validaReaccionR3V(){
          VerReaccionR3V = new ImageView(VerReaccionR3);
        ImageView VerReaccionR3VV = new ImageView(VerReaccionR3);
        if (foto.getReacciones().contains(Sistema.ListaReacciones.get(2))) {
            ReaccionesFoto.getChildren().addAll(VerReaccionR3VV);
        }
        VerReaccionR3V.setOnMouseClicked(RV3 -> {
            if (!foto.getReacciones().contains(Sistema.ListaReacciones.get(2))) {
                foto.anadirReaccion(Sistema.ListaReacciones.get(2));
                ReaccionesFoto.getChildren().add(VerReaccionR3VV);
            } else {
                foto.quitarReaccion(Sistema.ListaReacciones.get(2));
                ReaccionesFoto.getChildren().remove(VerReaccionR3VV);
            }
        });
        return VerReaccionR3V;
     }
      private ImageView validaReaccionR4V(){
          VerReaccionR4V = new ImageView(VerReaccionR4);
        ImageView VerReaccionR4VV = new ImageView(VerReaccionR4);
        if (foto.getReacciones().contains(Sistema.ListaReacciones.get(3))) {
            ReaccionesFoto.getChildren().addAll(VerReaccionR4VV);
        }
        VerReaccionR4V.setOnMouseClicked(RV4 -> {
            if (!foto.getReacciones().contains(Sistema.ListaReacciones.get(3))) {
                foto.anadirReaccion(Sistema.ListaReacciones.get(3));
                ReaccionesFoto.getChildren().add(VerReaccionR4VV);
            } else {
                foto.quitarReaccion(Sistema.ListaReacciones.get(3));
                ReaccionesFoto.getChildren().remove(VerReaccionR4VV);
            }
        });
        return VerReaccionR4V;
      }
      private ImageView validaReaccionR5V(){
         VerReaccionR5V = new ImageView(VerReaccionR5);
        ImageView VerReaccionR5VV = new ImageView(VerReaccionR5);
        if (foto.getReacciones().contains(Sistema.ListaReacciones.get(4))) {
            ReaccionesFoto.getChildren().addAll(VerReaccionR5VV);
        }
        VerReaccionR5V.setOnMouseClicked(RV5 -> {
            if (!foto.getReacciones().contains(Sistema.ListaReacciones.get(4))) {
                foto.anadirReaccion(Sistema.ListaReacciones.get(4));
                ReaccionesFoto.getChildren().add(VerReaccionR5VV);
            } else {
                foto.quitarReaccion(Sistema.ListaReacciones.get(4));
                ReaccionesFoto.getChildren().remove(VerReaccionR5VV);
            }
        });
        return VerReaccionR5V;
      }
      private ImageView validaReaccionR6V(){
          VerReaccionR6V = new ImageView(VerReaccionR6);
        ImageView VerReaccionR6VV = new ImageView(VerReaccionR6);
        if (foto.getReacciones().contains(Sistema.ListaReacciones.get(5))) {
            ReaccionesFoto.getChildren().addAll(VerReaccionR6VV);
        }
        VerReaccionR6V.setOnMouseClicked(RV6 -> {
            if (!foto.getReacciones().contains(Sistema.ListaReacciones.get(5))) {
                foto.anadirReaccion(Sistema.ListaReacciones.get(5));
                ReaccionesFoto.getChildren().add(VerReaccionR6VV);
            } else {
                foto.quitarReaccion(Sistema.ListaReacciones.get(5));
                ReaccionesFoto.getChildren().remove(VerReaccionR6VV);
            }
        });
        return VerReaccionR6V;
      }
    
    HBox ReaccionesFoto = new HBox();
    HBox HReacciones = new HBox();
    
    private void validaReacciones(){
        // Creo el contenedor para el nombre
        HReacciones = new HBox();
        HReacciones.setSpacing(20);
        HReacciones.setStyle("-fx-background-color: #ffffff;");
        HReacciones.setAlignment(Pos.CENTER);

        ReaccionesFoto = new HBox();
        ReaccionesFoto.setSpacing(20);
        ReaccionesFoto.setStyle("-fx-background-color: #ffffff;");
        Label LReacc = new Label("Reacciones: ");
        LReacc.setFont(Font.font("arial", FontWeight.EXTRA_BOLD, 20));
        ReaccionesFoto.getChildren().addAll(LReacc);

        VerReaccionR1 = new Image(Sistema.ListaReacciones.get(0).getUbicacion(), 50, 50, true, true);
        VerReaccionR1V = validaReaccionR1V();

        VerReaccionR2 = new Image(Sistema.ListaReacciones.get(1).getUbicacion(), 50, 50, true, true);
        VerReaccionR2V = validaReaccionR2V();

        VerReaccionR3 = new Image(Sistema.ListaReacciones.get(2).getUbicacion(), 50, 50, true, true);
        VerReaccionR3V = validaReaccionR3V();

        VerReaccionR4 = new Image(Sistema.ListaReacciones.get(3).getUbicacion(), 50, 50, true, true);
        VerReaccionR4V = validaReaccionR4V();

        VerReaccionR5 = new Image(Sistema.ListaReacciones.get(4).getUbicacion(), 50, 50, true, true);
        VerReaccionR5V = validaReaccionR5V();

        VerReaccionR6 = new Image(Sistema.ListaReacciones.get(5).getUbicacion(), 50, 50, true, true);
        VerReaccionR6V = validaReaccionR6V();

        HReacciones.getChildren().addAll(VerReaccionR1V, VerReaccionR2V, VerReaccionR3V, VerReaccionR4V, VerReaccionR5V, VerReaccionR6V);
    }
    
    private ScrollPane crearCuerpoF() {
        // Creo el contenedor principal
        VBox Vtotal = new VBox();
        Vtotal.setSpacing(35);
        Vtotal.setAlignment(Pos.CENTER);
        PanelBarra = new ScrollPane(Vtotal);
        PanelBarra.setStyle("-fx-border-color: black");
        PanelBarra.setFitToHeight(true);
        PanelBarra.setFitToWidth(true);
        PanelBarra.setStyle("-fx-background: #0BB9FF;");

        //Visor Foto
        VisualizarFoto = null;
        if (foto.getPathFoto().contains("file:")) {
            VisualizarFoto = new Image(foto.getPathFoto(), 600, 600, true, true);
        } else {
            VisualizarFoto = new Image("file:" + foto.getPathFoto(), 600, 600, true, true);
        }
        VisualizarFotoV = new ImageView(VisualizarFoto);

        validaReacciones();
        
        HBox Hashtags = validaHastag();

        //Creo el contenedor para la descripcion
        HBox Hintegrantes = validaHIntegrantes();

        //Creo el contenedor para la descripcion
        HBox Hdescripcion = validaHDescripcion();

        //Creo el contenedor para el lugar
        HBox HLugar = validaHLugar();

        //Creo el contenedor para la camara
        HBox Hcamara = validaHCamara();

        //Creo el contenedo para los botones
        HBox Hbotones = validaHBotones();

        //Agrego todos los contenedor al conetenedor principal
        Vtotal.getChildren().addAll(VisualizarFotoV, HReacciones, ReaccionesFoto, Hashtags, Hdescripcion, Hintegrantes, HLugar, Hcamara, Hbotones);

//      Les asigno la posicion a acada label y textbox
        AnchorPane.setLeftAnchor(HReacciones, CONSTANTES.EspacioRight);
        return PanelBarra;
    }

    public void EditarInfo() {
        if (!(CDatoscamara.getValue() == null)) {
            foto.setCamara(((Camara) CDatoscamara.getValue()));
        } else {
            foto.setCamara(foto.getCamara());
        }
        foto.setDescripcion(LDatosdescr.getText());
        ArrayList<String> names = new ArrayList();
        for (String nombres : LdatosInte.getText().split(",")) {
            names.add(nombres);
        }
        foto.setIntegrantes(names);
        ArrayList<String> hasht = new ArrayList();
        for (String ht : LDatosHash.getText().split(",")) {
            hasht.add(ht);
        }
        foto.setHashTag(hasht);
        foto.setLugar(LDatoslUGAR.getText());
    }

    public Button getGuardar() {
        return Guardar;
    }

    public void setGuardar(Button Guardar) {
        this.Guardar = Guardar;
    }

    public Button getCancelar() {
        return Cancelar;
    }

    public void setCancelar(Button Cancelar) {
        this.Cancelar = Cancelar;
    }

    public BorderPane getRootFormulario() {
        return rootFormulario;
    }

    public void setRootFormulario(BorderPane rootFormulario) {
        this.rootFormulario = rootFormulario;
    }

    public Button getAtras() {
        return Atras;
    }

    public void setAtras(Button Atras) {
        this.Atras = Atras;
    }

    public Button getBfoto() {
        return Bfoto;
    }

    public void setBfoto(Button Bfoto) {
        this.Bfoto = Bfoto;
    }

    public String getRutaFoto() {
        return RutaFoto;
    }

    public void setRutaFoto(String RutaFoto) {
        this.RutaFoto = RutaFoto;
    }

    public Image getPortadaAlbum() {
        return PortadaAlbum;
    }

    public void setPortadaAlbum(Image PortadaAlbum) {
        this.PortadaAlbum = PortadaAlbum;
    }

    public ImageView getFotoS() {
        return FotoS;
    }

    public void setFotoS(ImageView FotoS) {
        this.FotoS = FotoS;
    }

    public Foto getFoto() {
        return foto;
    }

    public void setFoto(Foto foto) {
        this.foto = foto;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz;

import Modelo.Foto.Camara;
import Modelo.Foto.Foto;
import Modelo.Foto.Reaccion;
import Modelo.Herramientas.CONSTANTES;
import Modelo.Herramientas.ManejoDeArchivos;
import Modelo.ModeloGaleria.Album;
import Modelo.Sistema;
import Modelo.Usuario.Usuario;
import java.io.File;
import java.net.URL;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.xml.soap.Node;

/**
 *
 * @author
 */
public class InterfazUsuario {

    private BorderPane root;
    private Button BAlbumes;
    private Button BFotos;
    private Button MisCamaras;
    private Button botonBuscar;
    private DatePicker FechaInicio;
    private DatePicker FechaFin;
    private TextField TNombreAlbum;
    private Usuario UsuarioActual;
    private VBox Vtotal;
    private ComboBox CBAlbumes;
    private ScrollPane PanelBarra;
    private Image fondoIntro;
    private ComboBox CFiltros;
    private ArrayList<String> OpcionesFiltrado;
    private ArrayList<HBox> TarjetasEliminar;
    private Button botonSlideshow;

    public InterfazUsuario(Usuario usuarioActual) {
        this.UsuarioActual = usuarioActual;
        TarjetasEliminar = new ArrayList();
        root = new BorderPane();
        fondoIntro = new Image(CONSTANTES.RUTA_IMAGENES + "fondoinicio.jpg", 800, 600, false, true);
        root.setBackground(new Background(new BackgroundImage(fondoIntro, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT, new BackgroundSize(800, 600, true, true, true, true))));
        root.setPrefSize(CONSTANTES.ANCHOPANTALLADEFAULT, CONSTANTES.ALTOPANTALLADEFAULT);
        root.setTop(crearTop());
        root.setCenter(bienvenidaAdmin());
        accionesBotones();
    }

    /**
     * Crea el top de la ventana del administrador
     *
     * @return HBox
     */
    private HBox crearTop() {
        HBox vOpcionesTop = new HBox();
        vOpcionesTop.setSpacing(2);
        vOpcionesTop.setAlignment(Pos.TOP_CENTER);

        BAlbumes = new Button("Ver Álbumes");
        BAlbumes.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");

        BFotos = new Button("Ver Fotos");

        BFotos.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");
        MisCamaras = new Button("Mis Camaras");
        MisCamaras.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");

        vOpcionesTop.getChildren().addAll(BAlbumes, BFotos, MisCamaras);

        return vOpcionesTop;
    }

    /**
     * Crea la bienvenida al administrador
     *
     * @return VBox
     */
    private VBox bienvenidaAdmin() {
        VBox hBien = new VBox();
        hBien.setSpacing(20);
        hBien.setAlignment(Pos.CENTER);
        Label lBien = new Label("¡Bienvenido " + UsuarioActual.getAlias() + "!");
        lBien.setFont(Font.font("Harlow Solid Italic", FontWeight.BOLD, 80));
        lBien.setTextFill(Color.WHITE);
        Image kImage = new Image(CONSTANTES.RUTA_IMAGENES + "inicio2.gif");
        ImageView kv = new ImageView(kImage);
        hBien.getChildren().addAll(lBien, kv);
        return hBien;
    }

    /**
     * Asigna las acciones a los botones superiores de la ventana
     */
    private void accionesBotones() {
        BAlbumes.setOnAction(e -> {
            try {
                root.setCenter(opcionAlbum());
            } catch (ParseException ex) {
                Logger.getLogger(InterfazUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        BFotos.setOnAction(e -> {
            try {
                root.setCenter(opcionFotos());
            } catch (ParseException ex) {
                Logger.getLogger(InterfazUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        MisCamaras.setOnAction(e -> {
            try {
                root.setCenter(opcionCamara());
            } catch (ParseException ex) {
                Logger.getLogger(InterfazUsuario.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    private Button valida(){
        Button bAlbum = new Button("Nuevo Album");
        Image imadd = new Image(CONSTANTES.RUTA_IMAGENES + "add.png", 18, 18, true, true);
        ImageView imaddv = new ImageView(imadd);
        bAlbum.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");
        bAlbum.setGraphic(imaddv);
        bAlbum.setOnAction(e -> {
            VentanaNuevoAlbum ventanaNuevoA = new VentanaNuevoAlbum();
            Scene scene1 = new Scene(ventanaNuevoA.getRootFormulario(), 450, 700);
            Stage nuevaVentana = new Stage();
            nuevaVentana.initStyle(StageStyle.TRANSPARENT);
            nuevaVentana.initModality(Modality.APPLICATION_MODAL);
            nuevaVentana.setResizable(false);
            nuevaVentana.setTitle("Nuevo Album");
            nuevaVentana.getIcons().add(new Image(CONSTANTES.RUTA_IMAGENES + "logo.png"));
            nuevaVentana.setScene(scene1);
            nuevaVentana.show();
            ventanaNuevoA.getCancelar().setOnAction(e3 -> {
                nuevaVentana.close();
            });
        ventanaNuevoA.getGuardar().setOnAction((ActionEvent eGN) -> {

                if (ventanaNuevoA.getTnombre().getText().equals("") && ventanaNuevoA.getTDescrip().getText().equals("")) {
                    Alert dialogoAlerta = new Alert(Alert.AlertType.WARNING);
                    dialogoAlerta.setTitle("¡Error Al Crear Album!");
                    Image imaviso = new Image(CONSTANTES.RUTA_IMAGENES + "cam.gif", 250, 250, true, true);
                    ImageView imavisov = new ImageView(imaviso);
                    dialogoAlerta.setGraphic(imavisov);
                    dialogoAlerta.setHeaderText("No se pudo crear el nuevo album en el sistema");
                    dialogoAlerta.setContentText("¡Debe llenar todos los campos del formulario!");
                    dialogoAlerta.initStyle(StageStyle.UTILITY);
                    java.awt.Toolkit.getDefaultToolkit().beep();
                    dialogoAlerta.showAndWait();
                } else {
                    Album nuevoAlbum = new Album(ventanaNuevoA.getTnombre().getText(), UsuarioActual.getIdUsuario());
                    if (!ventanaNuevoA.getRutaFoto().equals("")) {
                        nuevoAlbum.setPathPortada(ventanaNuevoA.getRutaFoto());
                        nuevoAlbum.setDescripcionAlbum(ventanaNuevoA.getTDescrip().getText());
                        UsuarioActual.getAlbumes().add(nuevoAlbum);
                        ManejoDeArchivos.guardarUsuarios(Sistema.ListaUsuarios);
                        nuevaVentana.close();
                    } else {
                        nuevoAlbum.setPathPortada(CONSTANTES.RUTA_IMAGENES + "album.png");
                        nuevoAlbum.setDescripcionAlbum(ventanaNuevoA.getTDescrip().getText());
                        UsuarioActual.getAlbumes().add(nuevoAlbum);
                        ManejoDeArchivos.guardarUsuarios(Sistema.ListaUsuarios);
                        nuevaVentana.close();
                    }
                }
            });
        });
        return bAlbum;
    }
    
    /**
     * Crea la interfaz perteneciente al album
     *
     *
     * @return VBox
     * @throws ParseException
     */
    private synchronized VBox opcionAlbum() throws ParseException {
        Vtotal = new VBox();
        Vtotal.setSpacing(35);
        AnchorPane vSegundasOpciones = new AnchorPane();
        Button bAlbum = valida();
        

        HBox hFechas = new HBox();
        hFechas.setSpacing(50);
        hFechas.setAlignment(Pos.CENTER);

        HBox hFinicio = new HBox();
        hFinicio.setSpacing(15);
        Label FInicio = new Label("Fecha Inicio: ");
        FInicio.setFont(Font.font("arial", FontWeight.BOLD, 25));
        FechaInicio = new DatePicker(LocalDate.now());
        FechaInicio.setEditable(false);
        FechaInicio.setPrefSize(180, 20);

        hFinicio.getChildren().addAll(FInicio, FechaInicio);

        HBox HFfin = new HBox();
        HFfin.setSpacing(15);
        Label Ffin = new Label("Fecha Fin: ");
        Ffin.setFont(Font.font("arial", FontWeight.BOLD, 25));
        FechaFin = new DatePicker(LocalDate.now());
        FechaFin.setEditable(false);
        FechaFin.setPrefSize(180, 20);
        HFfin.getChildren().addAll(Ffin, FechaFin);

        hFechas.getChildren().addAll(hFinicio, HFfin);

        HBox HNombreAlbum = new HBox();
        HNombreAlbum.setAlignment(Pos.CENTER);
        Label Lestado = new Label("Nombre: ");
        Lestado.setFont(Font.font("arial", FontWeight.BOLD, 25));
        TNombreAlbum = new TextField("");
        TNombreAlbum.setPromptText("Ingrese el nombre del Album.");
        TNombreAlbum.setPrefSize(180, 20);
        HNombreAlbum.getChildren().addAll(Lestado, TNombreAlbum);

        botonBuscar = new Button("Buscar");
        botonBuscar.setAlignment(Pos.TOP_RIGHT);
        botonBuscar.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");
        Image imaf = new Image(CONSTANTES.RUTA_IMAGENES + "find.png", 18, 18, true, true);
        ImageView imafv = new ImageView(imaf);
        botonBuscar.setGraphic(imafv);

        AnchorPane.setRightAnchor(botonBuscar, CONSTANTES.EspacioRight);
        AnchorPane.setLeftAnchor(bAlbum, CONSTANTES.EspacioRight);
        vSegundasOpciones.getChildren().addAll(bAlbum);

        FlowPane ContenedorAlbumes = new FlowPane();
        ContenedorAlbumes.setHgap(20);
        ContenedorAlbumes.setVgap(20);
        for (Album a : UsuarioActual.getAlbumes()) {
            HBox TarjetaAlbum = new HBox();
            TarjetaAlbum.setStyle("-fx-background-color: #ffffff;");
            Image FotoAlbum = null;
            if (a.getPathPortada().equals(CONSTANTES.RUTA_IMAGENES + "album.png")) {
                FotoAlbum = new Image(a.getPathPortada(), 150, 150, true, true);
            } else {
                FotoAlbum = new Image("file:" + a.getPathPortada(), 150, 150, true, true);
            }
            ImageView fotocview = new ImageView(FotoAlbum);
            Button Bt = new Button();
            Bt.setContentDisplay(ContentDisplay.RIGHT);
            Bt.setWrapText(true);
            Bt.setText("Nombre: " + a.getNombreAlbum() + "\nN° Fotos: "
                    + a.getFotos().size() + "\nDescripcion: " + a.getDescripcionAlbum() + "\nFecha Creación: " + a.getFechaCreacion().toLocalDate().toString());
            Bt.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 12px;");

            Image imeliminar = new Image(CONSTANTES.RUTA_IMAGENES + "eliminar.png", 100, 100, true, true);
            ImageView imeliminarv = new ImageView(imeliminar);
            Button eliminar = new Button("Eliminar Album");
            eliminar.setStyle("-fx-text-fill: BLUE; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 12px;");
            eliminar.setGraphic(imeliminarv);
            eliminar.setContentDisplay(ContentDisplay.TOP);
            eliminar.setOnAction(nuevoEliminar -> {
                ContenedorAlbumes.getChildren().remove(TarjetaAlbum);
                UsuarioActual.getAlbumes().remove(a);
                ManejoDeArchivos.guardarUsuarios(Sistema.ListaUsuarios);
            });

            Image editc = new Image(CONSTANTES.RUTA_IMAGENES + "edit.png", 100, 100, true, true);
            ImageView editcv = new ImageView(editc);
            Button edit = new Button("Editar Album");
            edit.setStyle("-fx-text-fill: BLUE; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 12px;");
            edit.setGraphic(editcv);
            edit.setContentDisplay(ContentDisplay.TOP);
            edit.setOnAction(nuevoC -> {
                VentanaEditarAlbum VentanaEditarAlbum = new VentanaEditarAlbum(a);
                Scene scene1 = new Scene(VentanaEditarAlbum.getRootFormulario(), 450, 750);
                Stage editarAlbum = new Stage();
                editarAlbum.initStyle(StageStyle.TRANSPARENT);
                editarAlbum.initModality(Modality.APPLICATION_MODAL);
                editarAlbum.setResizable(false);
                editarAlbum.setTitle("Editar Album");
                editarAlbum.getIcons().add(new Image(CONSTANTES.RUTA_IMAGENES + "logo.png"));
                editarAlbum.setScene(scene1);
                editarAlbum.show();
                VentanaEditarAlbum.getCancelar().setOnAction(e2 -> {
                    editarAlbum.close();
                });
                VentanaEditarAlbum.getGuardar().setOnAction(e4 -> {
                    if (!(VentanaEditarAlbum.getTnombre().equals(""))) {
                        a.setNombreAlbum(VentanaEditarAlbum.getTnombre().getText());
                        if (!VentanaEditarAlbum.getRutaFoto().equals("")) {
                            a.setPathPortada(VentanaEditarAlbum.getRutaFoto());
                        }
                        ManejoDeArchivos.guardarUsuarios(Sistema.ListaUsuarios);
                        //ManejoArchivos.guardarUsuarios(Usuarios);
                        editarAlbum.close();
                    } else {
                        Alert dialogoAlerta = new Alert(Alert.AlertType.INFORMATION);
                        dialogoAlerta.setTitle("Parametros Incorrectos");
                        dialogoAlerta.setHeaderText("Error de Ingreso de datos");
                        dialogoAlerta.setContentText("Debe llenar todos los campos."
                                + "Intente Nuevamente...");
                        Image K = new Image(CONSTANTES.RUTA_IMAGENES + "cam.gif");
                        ImageView kv = new ImageView(K);
                        dialogoAlerta.setGraphic(kv);
                        dialogoAlerta.initStyle(StageStyle.UTILITY);
                        java.awt.Toolkit.getDefaultToolkit().beep();
                        dialogoAlerta.showAndWait();
                    }

                });
            });
            TarjetaAlbum.getChildren().addAll(fotocview, Bt, edit, eliminar);
            ContenedorAlbumes.getChildren().addAll(TarjetaAlbum);
        }

        Vtotal.getChildren().addAll(vSegundasOpciones, ContenedorAlbumes);

        return Vtotal;
    }
    

    /**
     * Crea la interfaz perteneciente a las fotos
     *
     * @return
     * @throws ParseException
     */
    private synchronized VBox opcionFotos() throws ParseException {
        Vtotal = new VBox();
        Vtotal.setSpacing(35);

        HBox HOpcionAbum = new HBox();
        Label Limg = new Label("Album: ");
        Limg.setFont(Font.font("arial", FontWeight.BOLD, 25));
        ObservableList<Album> OLalbumes = FXCollections.observableArrayList(UsuarioActual.getAlbumes());
        CBAlbumes = new ComboBox(OLalbumes);
        CBAlbumes.setPromptText("Seleccione un Album");
        CBAlbumes.setStyle("-fx-text-fill: WHITE; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 15px;");
        HOpcionAbum.getChildren().addAll(Limg, CBAlbumes);

        VBox HBienFotos = new VBox();
        HBienFotos.setSpacing(20);
        HBienFotos.setAlignment(Pos.CENTER);
        Label LBienFotos = new Label(UsuarioActual.getAlias() + " ¡Elije el album deseado!");
        LBienFotos.setFont(Font.font("Harlow Solid Italic", FontWeight.BOLD, 80));
        LBienFotos.setTextFill(Color.WHITE);
        Image Inifotos = new Image(CONSTANTES.RUTA_IMAGENES + "IniFotos.gif");
        ImageView Inifotosv = new ImageView(Inifotos);
        HBienFotos.getChildren().addAll(LBienFotos, Inifotosv);
        Vtotal.getChildren().addAll(HOpcionAbum, HBienFotos);

        CBAlbumes.setOnAction((Event Eseleccionar) -> {
            if (!(CBAlbumes.getValue() == null)) {
                Vtotal.getChildren().clear();
                FlowPane ContenedorFotos = new FlowPane();

                HOpcionAbum.getChildren().clear();
                Limg.setText("Álbumes disponibles: ");
                CBAlbumes.setPromptText("Clic aquí para cambiar de Álbum");
                CBAlbumes.setStyle("-fx-text-fill: WHITE; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 15px;");
                HOpcionAbum.getChildren().addAll(Limg, CBAlbumes);

                AnchorPane VSegundasOpciones = new AnchorPane();
                Button BNAlbum = new Button("Nueva Foto");
                Image imadd = new Image(CONSTANTES.RUTA_IMAGENES + "add.png", 18, 18, true, true);
                ImageView imaddv = new ImageView(imadd);
                BNAlbum.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");
                BNAlbum.setGraphic(imaddv);
                BNAlbum.setOnAction(e -> {
                    llenaVentanaOpcionFotos(UsuarioActual,CBAlbumes,Sistema.ListaUsuarios);
                    
                });

                HBox HNombreFoto = new HBox();
                HNombreFoto.setAlignment(Pos.CENTER);
                Label Lestado = new Label("Parámetro: ");
                Lestado.setFont(Font.font("arial", FontWeight.BOLD, 25));
                TNombreAlbum = new TextField("");
                TNombreAlbum.setPromptText("Ingrese el parámetro de Filtrado.");
                TNombreAlbum.setPrefSize(180, 20);
                HNombreFoto.getChildren().addAll(Lestado, TNombreAlbum);

                HBox HOpcionesFiltro = new HBox();
                Label Lfiltr = new Label("Opciones de Filtrado: ");
                Lfiltr.setFont(Font.font("arial", FontWeight.BOLD, 25));
                OpcionesFiltrado = new ArrayList();
                OpcionesFiltrado.add("HashTag");
                OpcionesFiltrado.add("Descripcion");
                OpcionesFiltrado.add("Marca");
                OpcionesFiltrado.add("Modelo");
                OpcionesFiltrado.add("Reacciones");
                ObservableList<String> OFiltra = FXCollections.observableArrayList(OpcionesFiltrado);
                CFiltros = new ComboBox(OFiltra);
                CFiltros.setPromptText("Seleccione una opcion de filtrado");
                CFiltros.setStyle("-fx-text-fill: WHITE; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 15px;");
                HOpcionesFiltro.getChildren().addAll(Lfiltr, CFiltros);

                HBox TercerasOpciones = new HBox();
                TercerasOpciones.setAlignment(Pos.CENTER);
                TercerasOpciones.getChildren().addAll(HOpcionesFiltro, HNombreFoto);

                botonBuscar = new Button("Buscar");
                botonBuscar.setAlignment(Pos.TOP_RIGHT);
                botonBuscar.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");
                Image imaf = new Image(CONSTANTES.RUTA_IMAGENES + "find.png", 18, 18, true, true);
                ImageView imafv = new ImageView(imaf);
                botonBuscar.setGraphic(imafv);

                botonSlideshow = new Button("SlideShow");
                botonSlideshow.setAlignment(Pos.TOP_RIGHT);
                botonSlideshow.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");
                Image imaslid = new Image(CONSTANTES.RUTA_IMAGENES + "slide.png", 18, 18, true, true);
                ImageView imaslidv = new ImageView(imaslid);
                botonSlideshow.setGraphic(imaslidv);

                botonSlideshow.setOnAction(e -> {
                    VentanaSlideShow Vslide = new VentanaSlideShow(((Album) CBAlbumes.getValue()));
                    Scene scene1 = new Scene(Vslide.getRootFormulario(), 800, 1000);
                    Stage vsl = new Stage();
                    // vsl.initStyle(StageStyle.TRANSPARENT);
                    //vsl.initModality(Modality.APPLICATION_MODAL);
                    vsl.setResizable(false);
                    vsl.setTitle("SlideShow");
                    vsl.getIcons().add(new Image(CONSTANTES.RUTA_IMAGENES + "logo.png"));
                    vsl.setScene(scene1);
                    vsl.show();
                    Vslide.getAtras().setOnAction(ea -> {
                        vsl.close();
                        Vslide.getMediaPlayer().stop();
                        Vslide.setBandera(false);
                    });
                }
                );

                AnchorPane.setRightAnchor(botonSlideshow, CONSTANTES.EspacioRight * 100);
                AnchorPane.setRightAnchor(botonBuscar, CONSTANTES.EspacioRight);
                AnchorPane.setLeftAnchor(BNAlbum, CONSTANTES.EspacioRight);
                VSegundasOpciones.getChildren().addAll(BNAlbum, botonSlideshow, botonBuscar);

                ContenedorFotos.setHgap(20);
                ContenedorFotos.setVgap(20);

               validaFiltrosFoto2(ContenedorFotos,HOpcionAbum,VSegundasOpciones,TercerasOpciones);

                ContenedorFotos = validaFiltrosBusqFoto(ContenedorFotos);
                

            }
        });

        return Vtotal;
    }

    public void validaFiltrosFoto2(FlowPane ContenedorFotos, HBox HOpcionAbum, AnchorPane VSegundasOpciones, HBox TercerasOpciones){
         if (((Album) CBAlbumes.getValue()).getFotos().size() > 0) {

                    for (Foto F : ((Album) CBAlbumes.getValue()).getFotos()) {
                        PanelBarra = new ScrollPane(ContenedorFotos);
                        PanelBarra.setStyle("-fx-border-color: green");
                        PanelBarra.setFitToHeight(true);
                        PanelBarra.setFitToWidth(true);
                        PanelBarra.setStyle("-fx-background: #FFFFFF;");
                        PanelBarra.setBackground(new Background(new BackgroundImage(fondoIntro, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                                BackgroundPosition.DEFAULT, new BackgroundSize(800, 600, true, true, true, true))));
                        HBox TarjetaFotos = new HBox();
                        TarjetaFotos.setStyle("-fx-background-color: #ffffff; -fx-border-color: #000000; ");

                        Image PortadaFoto = new Image("file:" + F.getPathFoto(), 300, 300, true, true);
                        if (F.getPathFoto().contains("file:")) {
                            PortadaFoto = new Image(F.getPathFoto(), 300, 300, true, true);
                        }
                        ImageView fotocview = new ImageView(PortadaFoto);

                        fotocview.setOnMouseClicked(eVerFoto -> {
                            VentanaVerFoto VentanaVerFoto = new VentanaVerFoto(F);
                            Scene sceneVisorFoto = new Scene(VentanaVerFoto.getRootFormulario(), 650, 1000);
                            Stage nuevaVentana = new Stage();
                            nuevaVentana.initModality(Modality.APPLICATION_MODAL);
                            nuevaVentana.setResizable(true);
                            nuevaVentana.setTitle("Visor Fotos");
                            nuevaVentana.getIcons().add(new Image(CONSTANTES.RUTA_IMAGENES + "logo.png"));
                            nuevaVentana.setScene(sceneVisorFoto);
                            nuevaVentana.show();
                            VentanaVerFoto.getAtras().setOnAction(eAtras -> {
                                nuevaVentana.close();
                            });

                        });
                        Button Bt = new Button();
                        Bt.setContentDisplay(ContentDisplay.RIGHT);
                        Bt.setWrapText(true);
                        Bt.setText("Descripcion: " + F.getDescripcion() + "\nCamara: "
                                + F.getCamara().toString() + "\nReacciones: " + F.getReacciones().toString() + "\nFecha Creación : " + F.getFechaCreación().toString());
                        Bt.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 12px;");
                        Image editc = new Image(CONSTANTES.RUTA_IMAGENES + "edit.png", 100, 100, true, true);
                        ImageView editcv = new ImageView(editc);
                        editcv.setOnMouseClicked(e -> {
                        });

                        Image imeliminar = new Image(CONSTANTES.RUTA_IMAGENES + "eliminar.png", 100, 100, true, true);
                        ImageView imeliminarv = new ImageView(imeliminar);
                        Button eliminar = new Button("Eliminar Foto");
                        eliminar.setStyle("-fx-text-fill: BLUE; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 12px;");
                        eliminar.setGraphic(imeliminarv);
                        eliminar.setContentDisplay(ContentDisplay.TOP);
                        eliminar.setOnAction(nuevoEliminar -> {
                            ContenedorFotos.getChildren().remove(TarjetaFotos);
                            UsuarioActual.quitarFoto(F);
                            ManejoDeArchivos.guardarUsuarios(Sistema.ListaUsuarios);
                        });

                        Image imcopy = new Image(CONSTANTES.RUTA_IMAGENES + "copy.png", 100, 100, true, true);
                        ImageView imcopyv = new ImageView(imcopy);
                        Button copiar = new Button("Copiar Foto");
                        copiar.setStyle("-fx-text-fill: BLUE; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 12px;");
                        copiar.setGraphic(imcopyv);
                        copiar.setContentDisplay(ContentDisplay.TOP);
                        copiar.setOnAction((ActionEvent nuevoCopiar) -> {
                            VentanaCopiar ventanacopiarfoto = new VentanaCopiar(UsuarioActual);
                            Scene sceneCopiar = new Scene(ventanacopiarfoto.getRootFormulario(), 650, 400);
                            Stage nuevaVentanac = new Stage();
                            nuevaVentanac.initModality(Modality.APPLICATION_MODAL);
                            nuevaVentanac.setResizable(true);
                            nuevaVentanac.setTitle("Copiar Fotos");
                            nuevaVentanac.getIcons().add(new Image(CONSTANTES.RUTA_IMAGENES + "logo.png"));
                            nuevaVentanac.setScene(sceneCopiar);
                            nuevaVentanac.show();
                            ventanacopiarfoto.getCancelar().setOnAction(e3 -> {
                                nuevaVentanac.close();
                            });
                            ventanacopiarfoto.getGuardar().setOnAction(eGN -> {
                                if (!(ventanacopiarfoto.getCBAlbumes().getValue() == null)) {
                                    ((Album) ventanacopiarfoto.getCBAlbumes().getValue()).anadirFoto(F);
                                    nuevaVentanac.close();
                                    ManejoDeArchivos.guardarUsuarios(Sistema.ListaUsuarios);

                                } else {
                                    Alert dialogoAlerta = new Alert(Alert.AlertType.WARNING);
                                    dialogoAlerta.setTitle("¡Error Al Copiar la foto!");
                                    Image imaviso = new Image(CONSTANTES.RUTA_IMAGENES + "cam.gif", 250, 250, true, true);
                                    ImageView imavisov = new ImageView(imaviso);
                                    dialogoAlerta.setGraphic(imavisov);
                                    dialogoAlerta.setHeaderText("No se pudo copiar la foto del album");
                                    dialogoAlerta.setContentText("¡Debe elegir un album de destino!");
                                    dialogoAlerta.initStyle(StageStyle.UTILITY);
                                    java.awt.Toolkit.getDefaultToolkit().beep();
                                    dialogoAlerta.showAndWait();
                                }
                            });

                        });

                        Button edit = new Button("Editar Foto");
                        edit.setStyle("-fx-text-fill: BLUE; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 12px;");
                        edit.setGraphic(editcv);
                        edit.setContentDisplay(ContentDisplay.TOP);
                        edit.setOnAction(nuevoC -> {
                            VentanaEditarFoto VEditarFoto = new VentanaEditarFoto(F, UsuarioActual);

                            Scene scene1 = new Scene(VEditarFoto.getRootFormulario(), 800, 950);
                            Stage editarFoto = new Stage();
                            editarFoto.initStyle(StageStyle.TRANSPARENT);
                            editarFoto.initModality(Modality.APPLICATION_MODAL);
                            editarFoto.setResizable(false);
                            editarFoto.setTitle("Editar Foto");
                            editarFoto.getIcons().add(new Image(CONSTANTES.RUTA_IMAGENES + "logo.png"));
                            editarFoto.setScene(scene1);
                            editarFoto.show();
                            VEditarFoto.getAtras().setOnAction(eEditFotoC -> {
                                editarFoto.close();
                            });
                            VEditarFoto.getGuardar().setOnAction(eEditFotoG -> {
                                VEditarFoto.EditarInfo();
                                ManejoDeArchivos.guardarUsuarios(Sistema.ListaUsuarios);
                                editarFoto.close();
                            });
                        });
                        TarjetaFotos.setId(String.valueOf(F.getIdFoto()));
                        TarjetasEliminar.add(TarjetaFotos);
                        TarjetaFotos.getChildren().addAll(fotocview, Bt, edit, eliminar, copiar);
                        ContenedorFotos.getChildren().addAll(TarjetaFotos);

                    }
                    Vtotal.getChildren().addAll(HOpcionAbum, VSegundasOpciones, TercerasOpciones, PanelBarra);
                } else {
                    Vtotal.getChildren().clear();
                    VBox HMensajeNoFoto = new VBox();
                    HMensajeNoFoto.setSpacing(20);
                    HMensajeNoFoto.setAlignment(Pos.CENTER);
                    Label Lnofoto = new Label(UsuarioActual.getAlias() + " ¡No existen fotos en este album!");
                    Lnofoto.setFont(Font.font("Harlow Solid Italic", FontWeight.BOLD, 80));
                    Lnofoto.setTextFill(Color.WHITE);
                    Image Ininofotos = new Image(CONSTANTES.RUTA_IMAGENES + "cam.gif");
                    ImageView Ininofotosv = new ImageView(Ininofotos);
                    HMensajeNoFoto.getChildren().addAll(Lnofoto, Ininofotosv);
                    Vtotal.getChildren().addAll(HOpcionAbum, HMensajeNoFoto);
                }
    }
    public FlowPane validaFiltrosBusqFoto(FlowPane ContenedorFotos){
        botonBuscar.setOnAction(eFilter -> {
            if (!((CFiltros.getValue()) == null)) {
                for (Foto F : ((Album) CBAlbumes.getValue()).getFotos()) {
                    if (((String) CFiltros.getValue()).toUpperCase().equals("HASHTAG")) {
                        if (!F.getHashTag().contains(TNombreAlbum.getText())) {
                            String identificador = String.valueOf(F.getIdFoto());
                            for (HBox cf : TarjetasEliminar) {
                                if (cf.getId().equals(identificador)) {
                                    ContenedorFotos.getChildren().remove(cf);
                                }
                            }
                        }
                    } else if (((String) CFiltros.getValue()).toUpperCase().equals("DESCRIPCION")) {
                        if (!F.getDescripcion().contains(TNombreAlbum.getText())) {
                            String identificador = String.valueOf(F.getIdFoto());
                            for (HBox cf : TarjetasEliminar) {
                                if (cf.getId().equals(identificador)) {
                                    ContenedorFotos.getChildren().remove(cf);
                                }
                            }
                        }
                    } else if (((String) CFiltros.getValue()).toUpperCase().equals("MARCA")) {
                        if (!F.getCamara().getMarca().toUpperCase().equals(TNombreAlbum.getText().toUpperCase())) {
                            String identificador = String.valueOf(F.getIdFoto());
                            for (HBox cf : TarjetasEliminar) {
                                if (cf.getId().equals(identificador)) {
                                    ContenedorFotos.getChildren().remove(cf);
                                }
                            }
                        }
                    } else if (((String) CFiltros.getValue()).toUpperCase().equals("MODELO")) {
                        if (!F.getCamara().getModelo().toUpperCase().equals(TNombreAlbum.getText().toUpperCase())) {
                            String identificador = String.valueOf(F.getIdFoto());
                            for (HBox cf : TarjetasEliminar) {
                                if (cf.getId().equals(identificador)) {
                                    ContenedorFotos.getChildren().remove(cf);
                                }
                            }
                        }

                    } else if (((String) CFiltros.getValue()).toUpperCase().equals("REACCIONES")) {
                        boolean bandera = false;
                        for (Reaccion r : F.getReacciones()) {
                            if (r.getNombre().toUpperCase().equals(TNombreAlbum.getText().toUpperCase())) {
                                bandera = true;
                            }
                        }
                        String identificador = String.valueOf(F.getIdFoto());
                        for (HBox cf : TarjetasEliminar) {
                            if (cf.getId().equals(identificador) && bandera == false) {
                                ContenedorFotos.getChildren().remove(cf);
                            }
                        }

                    } else {
                        Alert dialogoAlerta = new Alert(Alert.AlertType.WARNING);
                        dialogoAlerta.setTitle("¡Error Al Filtrar la busqueda!");
                        Image imaviso = new Image(CONSTANTES.RUTA_IMAGENES + "cam.gif", 250, 250, true, true);
                        ImageView imavisov = new ImageView(imaviso);
                        dialogoAlerta.setGraphic(imavisov);
                        dialogoAlerta.setHeaderText("No se pudo realizar la busqueda");
                        dialogoAlerta.setContentText("¡Debe elegir un parámetro de filtrado!");
                        dialogoAlerta.initStyle(StageStyle.UTILITY);
                        java.awt.Toolkit.getDefaultToolkit().beep();
                        dialogoAlerta.showAndWait();
                    }
                }
            } else {
                Alert dialogoAlerta = new Alert(Alert.AlertType.WARNING);
                dialogoAlerta.setTitle("¡Error Al Filtrar la busqueda!");
                Image imaviso = new Image(CONSTANTES.RUTA_IMAGENES + "cam.gif", 250, 250, true, true);
                ImageView imavisov = new ImageView(imaviso);
                dialogoAlerta.setGraphic(imavisov);
                dialogoAlerta.setHeaderText("No se pudo realizar la busqueda");
                dialogoAlerta.setContentText("¡Debe elegir un parámetro de filtrado!");
                dialogoAlerta.initStyle(StageStyle.UTILITY);
                java.awt.Toolkit.getDefaultToolkit().beep();
                dialogoAlerta.showAndWait();
            }

        });
        return ContenedorFotos;
    }
    public Button validaCreacionCamara(){
        Button BNAlbum = new Button("Nuevo Cámara");
        Image imadd = new Image(CONSTANTES.RUTA_IMAGENES + "add.png", 18, 18, true, true);
        ImageView imaddv = new ImageView(imadd);
        BNAlbum.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");
        BNAlbum.setGraphic(imaddv);
        BNAlbum.setOnAction(e -> {
            VentanaNuevaCamara VentanaNuevoA = new VentanaNuevaCamara();
            Scene scene1 = new Scene(VentanaNuevoA.getRootFormulario(), 450, 700);
            Stage nuevaVentana = new Stage();
            nuevaVentana.initStyle(StageStyle.TRANSPARENT);
            nuevaVentana.initModality(Modality.APPLICATION_MODAL);
            nuevaVentana.setResizable(false);
            nuevaVentana.setTitle("Nueva Cámara");
            nuevaVentana.getIcons().add(new Image(CONSTANTES.RUTA_IMAGENES + "logo.png"));
            nuevaVentana.setScene(scene1);
            nuevaVentana.show();
            VentanaNuevoA.getCancelar().setOnAction(e3 -> {
                nuevaVentana.close();
            });
            VentanaNuevoA.getGuardar().setOnAction((ActionEvent eGN) -> {

                if (VentanaNuevoA.getTnombre().getText().equals("") && VentanaNuevoA.getTmodelo().getText().equals("")) {
                    Alert dialogoAlerta = new Alert(Alert.AlertType.WARNING);
                    dialogoAlerta.setTitle("¡Error Al Crear la cámara!");
                    Image imaviso = new Image(CONSTANTES.RUTA_IMAGENES + "cam.gif", 250, 250, true, true);
                    ImageView imavisov = new ImageView(imaviso);
                    dialogoAlerta.setGraphic(imavisov);
                    dialogoAlerta.setHeaderText("No se pudo crear la nueva cámara");
                    dialogoAlerta.setContentText("¡Debe llenar todos los campos del formulario!");
                    dialogoAlerta.initStyle(StageStyle.UTILITY);
                    java.awt.Toolkit.getDefaultToolkit().beep();
                    dialogoAlerta.showAndWait();
                } else {
                    Camara cnueva = new Camara(VentanaNuevoA.getTnombre().getText(), VentanaNuevoA.getTmodelo().getText());
                    if (!VentanaNuevoA.getRutaFoto().equals("")) {
                        cnueva.setPathImagen(VentanaNuevoA.getRutaFoto());
                        UsuarioActual.getCamaras().add(cnueva);
                        ManejoDeArchivos.guardarUsuarios(Sistema.ListaUsuarios);
                        nuevaVentana.close();
                    } else {
                        cnueva.setPathImagen(CONSTANTES.RUTA_IMAGENES + "camarapre.png");
                        UsuarioActual.getCamaras().add(cnueva);
                        ManejoDeArchivos.guardarUsuarios(Sistema.ListaUsuarios);
                        nuevaVentana.close();
                    }
                }
            });
        });
        return BNAlbum;
    }
    
    /**
     * Crea la interfaz perteneciente a las camaras
     *
     * @return
     * @throws ParseException
     */
    private synchronized VBox opcionCamara() throws ParseException {
        Vtotal = new VBox();
        Vtotal.setSpacing(35);
        AnchorPane VSegundasOpciones = new AnchorPane();
        
        Button BNAlbum = validaCreacionCamara();
        
        HBox HNombreAlbum = new HBox();
        HNombreAlbum.setAlignment(Pos.CENTER);
        Label Lestado = new Label("Nombre: ");
        Lestado.setFont(Font.font("arial", FontWeight.BOLD, 25));
        TNombreAlbum = new TextField("");
        TNombreAlbum.setPromptText("Ingrese el nombre de la camara.");
        TNombreAlbum.setPrefSize(180, 20);
        HNombreAlbum.getChildren().addAll(Lestado, TNombreAlbum);

        botonBuscar = new Button("Buscar");
        botonBuscar.setAlignment(Pos.TOP_RIGHT);
        botonBuscar.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");
        Image imaf = new Image(CONSTANTES.RUTA_IMAGENES + "find.png", 18, 18, true, true);
        ImageView imafv = new ImageView(imaf);
        botonBuscar.setGraphic(imafv);

        AnchorPane.setRightAnchor(botonBuscar, CONSTANTES.EspacioRight);
        AnchorPane.setLeftAnchor(BNAlbum, CONSTANTES.EspacioRight);
        VSegundasOpciones.getChildren().addAll(BNAlbum);

        FlowPane ContenedorAlbumes = new FlowPane();
        ContenedorAlbumes.setHgap(20);
        ContenedorAlbumes.setVgap(20);
        for (Camara c : UsuarioActual.getCamaras()) {
            HBox TarjetaAlbum = new HBox();
            TarjetaAlbum.setStyle("-fx-background-color: #ffffff;");
            Image FotoAlbum = null;
            if (c.getPathImagen().equals(CONSTANTES.RUTA_IMAGENES + "camarapre.png")) {
                FotoAlbum = new Image(c.getPathImagen(), 150, 150, true, true);
            } else {
                FotoAlbum = new Image("file:" + c.getPathImagen(), 150, 150, true, true);
            }
            ImageView fotocview = new ImageView(FotoAlbum);
            Button Bt = new Button();
            Bt.setContentDisplay(ContentDisplay.RIGHT);
            Bt.setWrapText(true);
            Bt.setText("Marca: " + c.getMarca() + "\nModelo: " + c.getModelo());
            Bt.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 12px;");

            Image imeliminar = new Image(CONSTANTES.RUTA_IMAGENES + "eliminar.png", 100, 100, true, true);
            ImageView imeliminarv = new ImageView(imeliminar);
            Button eliminar = new Button("Eliminar Cámara");
            eliminar.setStyle("-fx-text-fill: BLUE; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 12px;");
            eliminar.setGraphic(imeliminarv);
            eliminar.setContentDisplay(ContentDisplay.TOP);
            eliminar.setOnAction(nuevoEliminar -> {
                ContenedorAlbumes.getChildren().remove(TarjetaAlbum);
                UsuarioActual.getCamaras().remove(c);
            });

            Image editc = new Image(CONSTANTES.RUTA_IMAGENES + "edit.png", 100, 100, true, true);
            ImageView editcv = new ImageView(editc);
            Button edit = new Button("Editar Cámara");
            edit.setStyle("-fx-text-fill: BLUE; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 12px;");
            edit.setGraphic(editcv);
            edit.setContentDisplay(ContentDisplay.TOP);
            edit.setOnAction(nuevoC -> {
                VentanaEditarCamara VentanaEditarAlbum = new VentanaEditarCamara(c);
                Scene scene1 = new Scene(VentanaEditarAlbum.getRootFormulario(), 450, 750);
                Stage editarAlbum = new Stage();
                editarAlbum.initStyle(StageStyle.TRANSPARENT);
                editarAlbum.initModality(Modality.APPLICATION_MODAL);
                editarAlbum.setResizable(false);
                editarAlbum.setTitle("Editar Cámara");
                editarAlbum.getIcons().add(new Image(CONSTANTES.RUTA_IMAGENES + "logo.png"));
                editarAlbum.setScene(scene1);
                editarAlbum.show();
                VentanaEditarAlbum.getCancelar().setOnAction(e2 -> {
                    editarAlbum.close();
                });
                VentanaEditarAlbum.getGuardar().setOnAction(e4 -> {
                    if (!(VentanaEditarAlbum.getTnombre().getText().equals("") && VentanaEditarAlbum.getTmodelo().getText().equals(""))) {
                        c.setMarca(VentanaEditarAlbum.getTnombre().getText());
                        c.setModelo(VentanaEditarAlbum.getTmodelo().getText());
                        if (!VentanaEditarAlbum.getRutaFoto().equals("")) {
                            c.setPathImagen(VentanaEditarAlbum.getRutaFoto());
                        }
                        ManejoDeArchivos.guardarUsuarios(Sistema.ListaUsuarios);
                        editarAlbum.close();
                    } else {
                        Alert dialogoAlerta = new Alert(Alert.AlertType.INFORMATION);
                        dialogoAlerta.setTitle("Parametros Incorrectos");
                        dialogoAlerta.setHeaderText("Error de Ingreso de datos");
                        dialogoAlerta.setContentText("Debe llenar todos los campos."
                                + "Intente Nuevamente...");
                        Image K = new Image(CONSTANTES.RUTA_IMAGENES + "cam.gif");
                        ImageView kv = new ImageView(K);
                        dialogoAlerta.setGraphic(kv);
                        dialogoAlerta.initStyle(StageStyle.UTILITY);
                        java.awt.Toolkit.getDefaultToolkit().beep();
                        dialogoAlerta.showAndWait();
                    }

                });
            });
            TarjetaAlbum.getChildren().addAll(fotocview, Bt, edit, eliminar);
            ContenedorAlbumes.getChildren().addAll(TarjetaAlbum);
        }

        Vtotal.getChildren().addAll(VSegundasOpciones, ContenedorAlbumes);

        return Vtotal;
    }

    public BorderPane getRoot() {
        return root;
    }

    private void llenaVentanaOpcionFotos(Usuario UsuarioActual, ComboBox CBAlbumes, ArrayList<Usuario> ListaUsuarios) {
        VentanaNuevaFoto VentanaNuevaFoto = new VentanaNuevaFoto(UsuarioActual, ((Album) CBAlbumes.getValue()));
        Scene scene1 = new Scene(VentanaNuevaFoto.getRootFormulario(), 450, 700);
        Stage nuevaVentana = new Stage();
        nuevaVentana.initStyle(StageStyle.TRANSPARENT);
        nuevaVentana.initModality(Modality.APPLICATION_MODAL);
        nuevaVentana.setResizable(false);
        nuevaVentana.setTitle("Nueva Foto");
        nuevaVentana.getIcons().add(new Image(CONSTANTES.RUTA_IMAGENES + "logo.png"));
        nuevaVentana.setScene(scene1);
        nuevaVentana.show();
        VentanaNuevaFoto.getAtras().setOnAction(e3 -> {
            nuevaVentana.close();
        });
        VentanaNuevaFoto.getGuardar().setOnAction(enuevaf -> {
            VentanaNuevaFoto.CrearNuevaFoto();
            ManejoDeArchivos.guardarUsuarios(Sistema.ListaUsuarios);
            nuevaVentana.close();
        });
    }

}

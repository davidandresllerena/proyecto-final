/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz;

import Modelo.Herramientas.CONSTANTES;
import Modelo.ModeloGaleria.Album;
import Modelo.Sistema;
import Modelo.Usuario.Usuario;
import java.io.File;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Grupo 7 (David Llerena,Pedro Conforme,Ariel Velez)
 */
public class VentanaCopiar {

    private BorderPane rootFormulario;
    private Button Guardar;
    private Button Cancelar;
    private Button Bfoto;
    private Button FSel;
    private TextField Tnombre;
    private TextField TDescrip;
    private FileChooser SelecFoto;
    private String RutaFoto = "";
    private Image PortadaAlbum;
    private ImageView FotoS;
    private ComboBox CBAlbumesCopiar;
    private Usuario UsuarioActual;

    public VentanaCopiar(Usuario UsuarioActual) {
        this.UsuarioActual = UsuarioActual;
        rootFormulario = new BorderPane();
        Image fondoIntro = new Image(CONSTANTES.RUTA_IMAGENES + "fondoinicio.jpg", 450, 750, false, true);
        rootFormulario.setBackground(new Background(new BackgroundImage(fondoIntro, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT, new BackgroundSize(450, 750, true, true, true, true))));
        rootFormulario.setStyle("-fx-border-color: white; -fx-border-width: 5; ");
        rootFormulario.setPrefSize(450, 750);
        rootFormulario.setTop(crearEncabezado());
        rootFormulario.setCenter(crearCuerpoF());
    }

    /**
     * Crea el encabezado de la ventana para crear un album
     *
     * @return HBox
     */
    private HBox crearEncabezado() {

        HBox Hencabezado = new HBox();
        Label Ltop = new Label("Copiar Foto");
        Ltop.setTextFill(Color.web("white"));
        Ltop.setFont(Font.font("arial", FontWeight.BOLD, 20));
        Hencabezado.setAlignment(Pos.CENTER);
        Hencabezado.getChildren().addAll(Ltop);
        Hencabezado.setPrefHeight(30);
        return Hencabezado;
    }

    /**
     * crea el contenido centrar de la ventana de creacion de un album
     *
     * @return VBox
     */
    @SuppressWarnings("empty-statement")
    private VBox crearCuerpoF() {
        // Creo el contenedor principal
        VBox Vtotal = new VBox();
        Vtotal.setSpacing(35);
        Vtotal.setAlignment(Pos.CENTER);

        HBox HOpcionAbum = new HBox();
        Label Limg = new Label("Álbumes disponibles: ");
        Limg.setFont(Font.font("arial", FontWeight.BOLD, 25));
        ObservableList<Album> OLalbumes2 = FXCollections.observableArrayList(UsuarioActual.getAlbumes());
        CBAlbumesCopiar = new ComboBox(OLalbumes2);
        CBAlbumesCopiar.setPromptText("Seleccione un Album");
        CBAlbumesCopiar.setStyle("-fx-text-fill: WHITE; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 15px;");
        HOpcionAbum.getChildren().addAll(Limg, CBAlbumesCopiar);

        // Creo el contenedo para los botones
        HBox Hbotones = new HBox();
        Hbotones.setSpacing(20);
        Guardar = new Button("Guardar Edicion");
        Image imguardar = new Image(CONSTANTES.RUTA_IMAGENES + "save.png", 18, 18, true, true);
        ImageView imguardarv = new ImageView(imguardar);
        Guardar.setGraphic(imguardarv);
        Guardar.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");
        Cancelar = new Button("Cancelar Edicion");
        Image imcancelar = new Image(CONSTANTES.RUTA_IMAGENES + "cancel.png", 18, 18, true, true);
        ImageView imcancelarv = new ImageView(imcancelar);
        Cancelar.setGraphic(imcancelarv);
        Cancelar.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold;-fx-background-color: #ffffff; -fx-font-size: 18px;");
        Hbotones.setAlignment(Pos.CENTER);
        Hbotones.getChildren().addAll(Guardar, Cancelar);

        //Agrego todos los contenedor al conetenedor principal
        Vtotal.getChildren().addAll(HOpcionAbum, Hbotones);
//        Les asigno la posicion a acada label y textbox
//        AnchorPane.setRightAnchor(Tnombre, CONSTANTES.EspacioRight);
//        AnchorPane.setRightAnchor(TDescrip, CONSTANTES.EspacioRight);
//        AnchorPane.setRightAnchor(FSel, CONSTANTES.EspacioRight * 2);
        AnchorPane.setLeftAnchor(HOpcionAbum, CONSTANTES.EspacioRight * 15);
        AnchorPane.setRightAnchor(CBAlbumesCopiar, CONSTANTES.EspacioRight * 15);
//        AnchorPane.setRightAnchor(FSel, CONSTANTES.EspacioRight * 18);
//        AnchorPane.setLeftAnchor(Lfoto, CONSTANTES.EspacioRight);
        return Vtotal;
    }

    public TextField getTDescrip() {
        return TDescrip;
    }

    public void setTDescrip(TextField TDescrip) {
        this.TDescrip = TDescrip;
    }

    public BorderPane getRootFormulario() {
        return rootFormulario;
    }

    public ComboBox getCBAlbumes() {
        return CBAlbumesCopiar;
    }

    public void setCBAlbumes(ComboBox CBAlbumes) {
        this.CBAlbumesCopiar = CBAlbumes;
    }

    public Button getGuardar() {
        return Guardar;
    }

    public Button getCancelar() {
        return Cancelar;
    }

    public TextField getTnombre() {
        return Tnombre;
    }

    public String getRutaFoto() {
        return RutaFoto;
    }

    public Button getBfoto() {
        return Bfoto;
    }

    public void setBfoto(Button Bfoto) {
        this.Bfoto = Bfoto;
    }

    public Button getFSel() {
        return FSel;
    }

    public void setFSel(Button FSel) {
        this.FSel = FSel;
    }

    public FileChooser getSelecFoto() {
        return SelecFoto;
    }

    public void setSelecFoto(FileChooser SelecFoto) {
        this.SelecFoto = SelecFoto;
    }

    public Image getImagenConductor() {
        return PortadaAlbum;
    }

    public void setImagenConductor(Image imagenConductor) {
        this.PortadaAlbum = imagenConductor;
    }

    public ImageView getFotoS() {
        return FotoS;
    }

    public void setFotoS(ImageView FotoS) {
        this.FotoS = FotoS;
    }

    public Image getPortadaAlbum() {
        return PortadaAlbum;
    }

    public void setPortadaAlbum(Image PortadaAlbum) {
        this.PortadaAlbum = PortadaAlbum;
    }

}
